@if (\App::environment('production'))
  <link rel="stylesheet" href="{{ mix('/css/libs.css') }}">
  <link rel="stylesheet" href="{{ mix('/css/theme.css') }}">
@else
  <link rel="stylesheet" href="{{ url('/css/libs.css') }}">
  <link rel="stylesheet" href="{{ url('/css/theme.css') }}">
@endif

<link rel="stylesheet" href="{{ mix('/css/app.css') }}">
