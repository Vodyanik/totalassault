@if (\App::environment('production'))
  <script src="{{ mix('/js/libs.js') }}"></script>
  <script src="{{ mix('/js/theme.js') }}"></script>
@else
  <script src="{{ url('/js/libs.js') }}"></script>
  <script src="{{ url('/js/theme.js') }}"></script>
@endif
<script src="{{ mix('/js/app.js') }}"></script>
