window.mLayout = (function () {
  let horMenu;
  let asideMenu;
  let asideMenuOffcanvas;
  let horMenuOffcanvas;

  const initStickyHeader = function () {
    const header = $('.m-header');
    const options = {
      offset: {},
      minimize:{},
    };

    if (header.data('minimize-mobile') === 'hide') {
      options.minimize.mobile = {};
      options.minimize.mobile.on = 'm-header--hide';
      options.minimize.mobile.off = 'm-header--show';
    } else {
      options.minimize.mobile = false;
    }

    if (header.data('minimize') === 'hide') {
      options.minimize.desktop = {};
      options.minimize.desktop.on = 'm-header--hide';
      options.minimize.desktop.off = 'm-header--show';
    } else {
      options.minimize.desktop = false;
    }

    if (header.data('minimize-offset')) {
      options.offset.desktop = header.data('minimize-offset');
    }

    if (header.data('minimize-mobile-offset')) {
      options.offset.mobile = header.data('minimize-mobile-offset');
    }

    header.mHeader(options);
  };

  // handle horizontal menu
  const initHorMenu = function () {
    const $headerMenu = $('#m_header_menu');
    // init aside left offcanvas
    horMenuOffcanvas = $headerMenu.mOffcanvas({
      class: 'm-aside-header-menu-mobile',
      overlay: true,
      close: '#m_aside_header_menu_mobile_close_btn',
      toggle: {
        target: '#m_aside_header_menu_mobile_toggle',
        state: 'm-brand__toggler--active',
      },
    });

    horMenu = $headerMenu.mMenu({
      // submenu modes
      submenu: {
        desktop: 'dropdown',
        tablet: 'accordion',
        mobile: 'accordion'
      },
      // resize menu on window resize
      resize: {
        desktop: function () {
          const headerNavWidth = $('#m_header_nav').width();
          const headerMenuWidth = $('#m_header_menu_container').width();
          const headerTopbarWidth = $('#m_header_topbar').width();
          const spareWidth = 20;

          return (headerMenuWidth + headerTopbarWidth + spareWidth) < headerNavWidth;
        },
      },
    });
  };

  // handle vertical menu
  const initLeftAsideMenu = function () {
    const menu = $('#m_ver_menu');

    // init aside menu
    const menuOptions = {
      // submenu setup
      submenu: {
        desktop: {
          // by default the menu mode set to accordion in desktop mode
          default: (menu.data('menu-dropdown') === true ? 'dropdown' : 'accordion'),
          // whenever body has this class switch the menu mode to dropdown
          state: {
            body: 'm-aside-left--minimize',
            mode: 'dropdown',
          },
        },
        tablet: 'accordion', // menu set to accordion in tablet mode
        mobile: 'accordion', // menu set to accordion in mobile mode
      },

      //accordion setup
      accordion: {
        autoScroll: true,
        expandAll: false,
      },
    };

    asideMenu = menu.mMenu(menuOptions);

    // handle fixed aside menu
    if (menu.data('menu-scrollable')) {
      function initScrollableMenu(obj) {
        const asideHeader = $('.m-aside-left .m-aside__header');
        const asideFooter = $('.m-aside-left .m-aside__footer');

        if (mUtil.isInResponsiveRange('tablet-and-mobile')) {
          // destroy if the instance was previously created
          mApp.destroyScroller(obj);
          return;
        }

        let height = mUtil.getViewPort().height - $('.m-header').outerHeight()
          - (asideHeader.length !== 0 ? asideHeader.outerHeight() : 0)
          - (asideFooter.length !== 0 ? asideFooter.outerHeight() : 0);
          //- $('.m-footer').outerHeight();

        // create/re-create a new instance
        mApp.initScroller(obj, { height });
      }

      initScrollableMenu(asideMenu);

      mUtil.addResizeHandler(() => {
        initScrollableMenu(asideMenu);
      });
    }
  };

  // handle vertical menu
  const initLeftAside = function () {
    // init aside left offcanvas
    const $asideLeft = $('#m_aside_left');
    let asideOffcanvasClass = 'm-aside-left--offcanvas-default';
    if (!$asideLeft.hasClass('m-aside-left--offcanvas-default')) {
      asideOffcanvasClass = 'm-aside-left';
    }

    asideMenuOffcanvas = $asideLeft.mOffcanvas({
      class: asideOffcanvasClass,
      overlay: true,
      close: '#m_aside_left_close_btn',
      toggle: {
        target: '#m_aside_left_offcanvas_toggle',
        state: 'm-brand__toggler--active'
      }
    });
  };

  // handle sidebar toggle
  const initLeftAsideToggle = function () {
    const asideLeftToggle = $('#m_aside_left_minimize_toggle').mToggle({
      target: 'body',
      targetState: 'm-brand--minimize m-aside-left--minimize',
      togglerState: 'm-brand__toggler--active'
    }).on('toggle', function(toggle) {
      horMenu.pauseDropdownHover(800);
      asideMenu.pauseDropdownHover(800);

      //== Remember state in cookie
      //Cookies.set('sidebar_toggle_state', toggle.getState());
    });

    //== Example: minimize the left aside on page load
    //== asideLeftToggle.toggleOn();

    $('#m_aside_left_hide_toggle').mToggle({
      target: 'body',
      targetState: 'm-aside-left--hide',
      togglerState: 'm-brand__toggler--active'
    }).on('toggle', () => {
      horMenu.pauseDropdownHover(800);
      asideMenu.pauseDropdownHover(800);
    });
  };

  const initTopbar = function () {
    const $notificationIcon = $('#m_topbar_notification_icon .m-nav__link-icon');
    const $notificationBadge = $('#m_topbar_notification_icon .m-nav__link-badge');

    $('#m_aside_header_topbar_mobile_toggle').click(() => {
      $('body').toggleClass('m-topbar--on');
    });

    // Animated Notification Icon
    setInterval(() => {
      $notificationIcon.addClass('m-animate-shake');
      $notificationBadge.addClass('m-animate-blink');
    }, 3000);

    setInterval(() => {
      $notificationIcon.removeClass('m-animate-shake');
      $notificationBadge.removeClass('m-animate-blink');
    }, 6000);
  };

  const initScrollTop = function () {
    $('[data-toggle="m-scroll-top"]').mScrollTop({
      offset: 300,
      speed: 600
    });
  };

  return {
    init: function () {
      this.initHeader();
      this.initAside();
    },

    initHeader: function () {
      initStickyHeader();
      initHorMenu();
      initTopbar();
      initScrollTop();
    },

    initAside: function () {
      initLeftAside();
      initLeftAsideMenu();
      initLeftAsideToggle();

      this.onLeftSidebarToggle(function (e) {
        const datatables = $('.m-datatable');
        $(datatables).each(function () {
          $(this).mDatatable('redraw');
        });
      });
    },

    getAsideMenu: function () {
      return asideMenu;
    },

    onLeftSidebarToggle: function(func) {
      $('#m_aside_left_minimize_toggle').mToggle().on('toggle', func);
    },

    closeMobileAsideMenuOffcanvas: function () {
      if (mUtil.isMobileDevice()) {
        asideMenuOffcanvas.hide();
      }
    },

    closeMobileHorMenuOffcanvas: function () {
      if (mUtil.isMobileDevice()) {
        horMenuOffcanvas.hide();
      }
    },
  };
}());
