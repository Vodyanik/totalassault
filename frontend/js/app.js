// import Vue
const Vue = require('vue');

// import all required files
import settings from 'miscellaneous/Imports';

const VeeValidate = require('vee-validate');

// create the main Vue instance
new Vue(settings);
