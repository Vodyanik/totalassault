import Vue from 'vue';
import VueI18n from 'vue-i18n';
import English from 'lang/en/index';

Vue.use(VueI18n);

const messages = {
  en: English
};

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en', // set locale
  messages: messages, // set locale messages
});

global.i18n = i18n;

export default i18n;
