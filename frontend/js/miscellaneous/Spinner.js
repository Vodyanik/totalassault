import DotLoader from 'vue-spinner/src/DotLoader.vue';
import Vue from 'vue';

Vue.component('dot-loader', DotLoader);
