import Vue from 'vue';
import { apiUrl, baseUrl } from '.env';

window.apiUrl = apiUrl;
window.baseUrl = baseUrl;
window.Bus = new Vue();
