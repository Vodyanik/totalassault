/**
 * Laravel like function for pluralization
 */
if (typeof window.lang !== 'function') {
  window.lang = message => global.i18n.t(message);
}
/**
 * Laravel like function to generate urls
 */
if (typeof window.url !== 'function') {
  window.url = (url, apiUrl = false) => {
    if (apiUrl) {
      return window.apiUrl + url;
    }

    return window.url + url;
  };
}

/**
 * Check if a variable is a string or not
 */
if (typeof window.isString !== 'function') {
  window.isString = variable => typeof variable === 'string';
}

/**
 * Check if a variable is an array or not
 */
if (typeof window.isArray !== 'function') {
  window.isArray = variable => typeof variable === 'string' && variable.constructor === Array;
}

/**
 * Create a filter object to use with the Bruno package
 */
if (typeof window.createFilter !== 'function') {
  window.createFilter = (tableField, operator, value) => {
    return {
      key: tableField,
      value,
      operator,
    };
  };
}

/**
 * Create a filter group object to use with the Bruno package
 */
if (typeof window.createFilterGroup !== 'function') {
  window.createFilterGroup = (filters = [], or = false) => {
    return {
      or,
      filters,
    };
  };
}

/**
 * Convert datetime to date string
 */
if (typeof window.dateTimeToDate !== 'function') {
  window.dateTimeToDate = (datetime) => {
    return moment(datetime).format('YYYY-MM-DD');
  };
}

/**
 * Convert a datetime string to a standard date format for each user
 */
if (typeof window.dateFormatStd !== 'function') {
  window.dateFormatStd = (datetime) => {
    // TODO: retrieve format for each user individually
    return moment(datetime).format('YYYY-MM-DD');
  };
}

