const MenuLinks = [
  {
    name: window.lang('menu.dashboard'),
    url: '/dashboard',
    icon: 'flaticon-line-graph',
  },
  {
    name: window.lang('menu.projects'),
    url: '',
    icon: 'flaticon-folder-1',
    children: [
      {
        name: window.lang('menu.activeProjects'),
        path: '/projects/active',
      },
      {
        name: window.lang('menu.archivedProjects'),
        path: '/projects/archived',
      },
      {
        name: window.lang('menu.create'),
        path: '/projects/create',
      },
    ],
  },
  {
    name: window.lang('menu.advertising'),
    url: '',
    icon: 'flaticon-statistics',
    children: [
      {
        name: window.lang('menu.campaigns'),
        path: '',
        children: [
          {
            name: window.lang('menu.active'),
            path: '/advertising/campaigns/active',
          },
          {
            name: window.lang('menu.create'),
            path: '/advertising/campaigns/create',
          },
        ],
      },
      {
        name: window.lang('menu.facebook'),
        path: '',
        children: [
          {
            name: window.lang('menu.dailyPerformance'),
            path: '/advertising/facebook/daily-performance',
          },
          {
            name: window.lang('menu.overview'),
            path: '/advertising/facebook/overview',
          },
        ],
      },
    ],
  },
  {
    name: window.lang('menu.testing'),
    url: '',
    icon: 'flaticon-network',
    children: [
      {
        name: window.lang('menu.rangePicker'),
        path: '/testing/rangepicker',
      },
      {
        name: window.lang('menu.charts'),
        path: '/testing/charts',
      },
    ],
  },
];
export default MenuLinks;
