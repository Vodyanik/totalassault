import 'mixins/Global';
import 'miscellaneous/Globals';
import 'miscellaneous/Functions';
import 'miscellaneous/Validator';
import 'miscellaneous/Spinner';
import Vue from 'vue';
import router from 'miscellaneous/Router';
import store from 'store/index';
import i18n from 'miscellaneous/Language';
import Auth from 'packages/auth/Auth';
import BootstrapVue from 'bootstrap-vue';
import MainPage from 'views/Main';

Vue.use(Auth);
Vue.use(BootstrapVue);

Vue.component('page-container', MainPage);

const settings = {
  el: '#app',
  router,
  store,
  i18n,
  template: '<page-container></page-container>',
};

export default settings;
