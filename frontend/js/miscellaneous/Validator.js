import VeeValidate from 'vee-validate';
import Vue from 'vue';
import lang from './Language';

const dictionary = {
  en: {
    attributes: lang.messages.en.dictionary,
  },
};


Vue.use(VeeValidate, {
  fieldsBagName: 'vee-fields',
  dictionary,
});
