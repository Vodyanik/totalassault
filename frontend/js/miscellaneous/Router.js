import Vue from 'vue';
import VueRouter from 'vue-router';
import Routes from 'routes';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  scrollBehavior: () => ({ y: 0 }),
  routes: Routes,
});

router.beforeEach((to, from, next) => {
  // redirect to the login page
  if (to.matched.some(record => record.meta.forAuth)) {
    if (!Vue.auth.isAuthenticated()) {
      next({ path: '/' });
    }
  }

  // redirect to dashboard
  if (to.matched.some(record => record.meta.forGuest)) {
    if (Vue.auth.isAuthenticated()) {
      next({ path: '/dashboard' });
    }
  }

  if (to.matched.some(record => record.meta.immediateLoad)) {
    window.Bus.$emit('page.ready');
  }

  next();
});

export default router;
