// base framework
require('../../resources/theme/framework/base/util');
require('../../resources/theme/framework/base/app');

// general components
require('../../resources/theme/framework/components/general/animate');
require('../../resources/theme/framework/components/general/dropdown');
require('../../resources/theme/framework/components/general/example');
require('../../resources/theme/framework/components/general/header');
require('../../resources/theme/framework/components/general/menu');
require('../../resources/theme/framework/components/general/messenger');
require('../../resources/theme/framework/components/general/offcanvas');
require('../../resources/theme/framework/components/general/portlet');
require('../../resources/theme/framework/components/general/quicksearch');
require('../../resources/theme/framework/components/general/scroll-top');
require('../../resources/theme/framework/components/general/toggle');
require('../../resources/theme/framework/components/general/wizard');

// demo
require('../../resources/theme/tara/layout');
require('../../resources/theme/snippets/base/quick-sidebar');

// dashboard
require('../../resources/theme/tara/dashboard');
