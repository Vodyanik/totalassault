export default function (Vue) {
  Vue.auth = {
    setUserData(user) {
      localStorage.setItem('tara.user', JSON.stringify(user));
    },
    getUserData() {
      const user = localStorage.getItem('tara.user');
      return JSON.parse(user);
    },
    destroyUserData() {
      localStorage.removeItem('tara.user');
    },
    setToken(token, expiration) {
      localStorage.setItem('token', token);
      localStorage.setItem('expiration', expiration);
    },
    getToken() {
      const token = localStorage.getItem('token');
      const expiration = localStorage.getItem('expiration');

      if (!token || !expiration) {
        return null;
      }

      if (Date.now() > parseInt(expiration, 10)) {
        this.destroyToken();
        return null;
      }

      return token;
    },
    destroyToken() {
      localStorage.removeItem('token');
      localStorage.removeItem('expiration');
    },
    isAuthenticated() {
      return this.getToken();
    },
  };

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get() {
        return Vue.auth;
      },
    },
  });
}
