import Full from 'views/Full';
import Login from 'views/auth/Login';
import Dashboard from 'views/dashboard/Index';
import UsersIndex from 'views/users/Index';
import UsersCreate from 'views/users/Create';
import UsersEdit from 'views/users/Edit';
import CampaignsActive from 'views/advertising/campaigns/Active';
import CampaignsCreate from 'views/advertising/campaigns/Create';
import CampaignEdit from 'views/advertising/campaigns/Edit';
import ActiveProjects from 'views/projects/Active';
import ArchivedProjects from 'views/projects/Archived';
import ProjectCreate from 'views/projects/Create';
import ProjectEdit from 'views/projects/Edit';
import FacebookOverview from 'views/advertising/facebook/Overview';
import FacebookDailyPerformance from 'views/advertising/facebook/DailyPerformance';
import RangePicker from 'views/testing/DataRangePicker';
import TestingCharts from 'views/testing/Charts';

const routes = [
  {
    path: '/',
    component: Login,
    meta: {
      forGuest: true,
      immediateLoad: true,
    },
  },
  {
    path: '/',
    name: 'Home',
    component: Full,
    meta: { forAuth: true },
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: {
          forAuth: true,
          immediateLoad: true,
        },
      },
      {
        path: '/users',
        name: 'Users',
        component: UsersIndex,
        meta: {
          forAuth: true,
          immediateLoad: true,
        },
      },
      {
        path: '/users/create',
        name: 'Create User',
        component: UsersCreate,
        meta: { forAuth: true },
      },
      {
        path: '/users/:id',
        name: 'Edit User',
        component: UsersEdit,
        meta: { forAuth: true },
      },
      {
        path: '/projects',
        name: 'Projects',
        redirect: '/projects/active',
        meta: { forAuth: true },
      },
      {
        path: '/projects/active',
        name: 'Active Projects',
        component: ActiveProjects,
        meta: { forAuth: true },
      },
      {
        path: '/projects/archived',
        name: 'Archived Projects',
        component: ArchivedProjects,
        meta: { forAuth: true },
      },
      {
        path: '/projects/create',
        name: 'Create Project',
        component: ProjectCreate,
        meta: {
          forAuth: true,
          immediateLoad: true,
        },
      },
      {
        path: '/projects/edit/:id',
        name: 'Edit Project',
        component: ProjectEdit,
        meta: { forAuth: true },
      },
      {
        path: '/advertising/campaigns/active',
        name: 'Active Facebook Campaign',
        component: CampaignsActive,
        meta: { forAuth: true },
      },
      {
        path: 'advertising/campaigns/create',
        name: 'Create Campaign',
        component: CampaignsCreate,
        meta: {
          forAuth: true,
          immediateLoad: true,
        },
      },
      {
        path: 'advertising/campaigns/edit/:id',
        name: 'Edit Campaign',
        component: CampaignEdit,
        meta: { forAuth: true },
      },
      {
        path: '/advertising/facebook/overview',
        name: 'Facebook Overview',
        component: FacebookOverview,
        meta: { forAuth: true },
      },
      {
        path: '/advertising/facebook/daily-performance',
        name: 'Facebook Daily Performance',
        component: FacebookDailyPerformance,
        meta: { forAuth: true },
      },
      {
        path: 'testing/rangepicker',
        name: 'Range Picker',
        component: RangePicker,
        meta: {
          forAuth: true,
          immediateLoad: true,
        },
      },
      {
        path: 'testing/charts',
        name: 'Charts',
        component: TestingCharts,
        meta: {
          forAuth: true,
          immediateLoad: true,
        },
      },
    ],
  },
];

export default routes;
