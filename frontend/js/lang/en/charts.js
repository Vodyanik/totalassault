export default {
  impressionsAndCpm: 'Impressions & CPM',
  linkClicksAndCplc: 'Link Clicks & CPLC',
  pageLikesAndLikeCost: 'Page Likes & Cost Per Page Like',
  costPerAction: 'Cost Per Action',
  impressions: 'Impressions',
  actions: 'Actions',
  linkClicksAndCost: 'Link Clicks & Cost Per Link Click',
  linkClicks: 'Link Clicks',
  costPerLinkClick: 'Cost Per Link Click',
  pageLikesAndCost: 'Page Likes & Cost Per Page Like',
  pageLikes: 'Page Likes',
  costPerPageLike: 'Cost Per Page Like',
};
