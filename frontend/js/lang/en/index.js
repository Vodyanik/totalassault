import login from './login';
import main from './main';
import menu from './menu';
import users from './users';
import projects from './projects';
import charts from './charts';
import adCampaigns from './advertising/campaigns';
import adFacebook from './advertising/facebook';

const en = {
  login,
  main,
  menu,
  users,
  adCampaigns,
  adFacebook,
  projects,
  charts,
};

export default en;
