export default {
  projects: 'Projects',
  projectManagement: 'Projects Management',
  createProject: 'Create Project',
  editProject: 'Edit Project',
  active: 'Active',
  storeSuccess: 'The project was stored',
  storeFail: 'The project wasn\'t stored',
  updateSuccess: 'The project was updated',
  updateFail: 'The project wasn\'t updated',
  destroySuccess: 'The project was deleted',
  destroyFail: 'The project was not deleted',
  pending: 'Pending',
  disabled: 'Disabled',
  deleteWarning: 'Once deleted, you will not be able to recover this project.',
  columns: 'Columns',
  visibleCols: 'Visible Columns',
  hiddenCols: 'Hidden Columns',
  activeProjects: 'Active Projects',
  archivedProjects: 'Archived Projects',
  customizeCols: 'Columns Customization',
  colsCustomize: 'You can add or remove your favorite columns for your perfect view.',
  saveChanges: 'Save Changes',
  name: 'Project',
  image: 'Image',
  slug: 'Slug Id',
  status: 'Status',
  projectStatus: 'Project Status',
  gallery: 'Project Gallery',
  facebook: 'Facebook',
  twitter: 'Twitter',
  instagram: 'Instagram',
  youtube: 'Youtube',
  assets: 'Assets',
  soundcloud: 'SoundCloud',
  publicity: 'Publicity Status',
  placeholderName: 'Enter name',
  placeholderImage: 'Please select an image',
  placeholderSlug: 'Enter slug',
  placeholderFacebook: 'Enter Facebook id',
  placeholderTwitter: 'Enter Twitter id',
  placeholderInstagram: 'Enter Instagram id',
  placeholderYoutube: 'Enter Youtube id',
  placeholderSoundcloud: 'Enter SoundCloud id',
  placeholderAssets: 'Assets',
  publicity_bc_team_id: 'Publicity Team ID',
};
