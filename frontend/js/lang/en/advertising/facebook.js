export default {
  dailyPerformance: 'Daily Performance',
  mainTitle: 'Campaign Overview',
  campaign: 'Campaign',
  cost: 'Cost',
  impressions: 'Impressions',
  date: 'Date',
  CPM: 'CPM',
  linkClicks: 'Link Clicks',
  CPLC: 'CPLC',
  videoViews: 'Video views(3s)',
  CPV: 'CPV(3s)',
};
