import state from 'store/users/State';
import actions from 'store/users/Actions';
import mutations from 'store/users/Mutations';
import getters from 'store/users/Getters';

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
