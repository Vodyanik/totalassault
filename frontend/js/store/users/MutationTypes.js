
export const LOAD_USER = 'LOAD_USER';
export const LOAD_USERS = 'LOAD_USERS';
export const UPDATE_USER = 'UPDATE_USER';

export const LOAD_USER_OK = 'LOAD_USER_OK';
export const LOAD_USERS_OK = 'LOAD_USERS_OK';
export const UPDATE_USER_OK = 'UPDATE_USER_OK';

export const LOAD_USER_FAIL = 'LOAD_USER_FAIL';
export const LOAD_USERS_FAIL = 'LOAD_USERS_FAIL';
export const UPDATE_USER_FAIL = 'UPDATE_USER_FAIL';
