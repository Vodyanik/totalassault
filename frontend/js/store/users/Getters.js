// the functions for retrieving the state
export default {
  user (state) {
    return state.user;
  },
  users (state) {
    return state.users;
  }
};
