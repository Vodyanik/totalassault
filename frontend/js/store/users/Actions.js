import * as types from 'store/users/MutationTypes';

// async functions that update the state
export default {
  /**
   * Get the users from the API
   *
   * @param commit
   * @param dispatch
   * @param params
   * @returns {Promise<any>}
   */
  loadUsers ({ commit, dispatch }, params) {
    //commit(types.LOAD_USERS);

    return new Promise((resolve, reject) => {
      axios.get(window.apiUrl + '/users', { params })
        .then(response => {
          commit(types.LOAD_USERS_OK, response.data);
          resolve(response);
        })
        .catch (response => {
          commit(types.LOAD_USERS_FAIL);
          reject(response.data);
        });
    });
  }
};
