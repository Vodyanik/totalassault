import * as types from "store/users/MutationTypes";

// the functions for updating the state
export default {
  [types.LOAD_USER_OK] (state, user) {
    state.user = user;
  },
  [types.LOAD_USER_FAIL] (state, user) {
    console.dir(user);
  },
  [types.LOAD_USERS_OK] (state, users) {
    state.users = users;
  },
  [types.LOAD_USERS_FAIL] (state, users) {
    console.dir(users);
  },
  [types.UPDATE_USER_OK] (state, user) {
    state.user = user;
  },
  [types.UPDATE_USER_FAIL] (state, user) {
    console.dir(user);
  }
};
