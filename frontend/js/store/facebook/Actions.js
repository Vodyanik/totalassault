import * as types from 'store/facebook/MutationTypes';

export default {
  /**
   * Retrieve the daily performance records (paginated)
   *
   * @param commit
   * @param params
   */
  loadDailyPerformance({ commit }, params) {
    const queryString = window.qs.stringify(params);

    return new Promise((resolve, reject) => {
      axios.get(window.url(`/advertising/facebook/daily-performance?${queryString}`, true))
        .then((response) => {
          commit(types.LOAD_FB_DAILY_PERFORMANCE, response.data);
          resolve(response);
        })
        .catch(response => reject(response.data));
    });
  },
  /**
   * Retrieve the archived projects (paginated)
   *
   * @param commit
   * @param params
   */
  loadArchivedProjects({ commit }, params) {
    /*
    const archiveFilter = window.createFilter('status', 'eq', 0);
    const filterGroup = window.createFilterGroup([archiveFilter]);

    if (window.isArray(params.filter_groups)) {
      params.filter_groups.push(filterGroup);
    } else {
      params.filter_groups = [filterGroup];
    }

    const queryString = window.qs.stringify(params);

    return new Promise((resolve, reject) => {
      axios.get(window.url(`/projects?${queryString}`, true))
        .then((response) => {
          commit(types.LOAD_PROJECTS, response.data);
          resolve(response);
        })
        .catch(response => reject(response.data));
    });
    */
  },
  /**
   * Retrieve an existing project
   *
   * @param commit
   * @param params
   */
  loadProject({ commit }, params) {
    return new Promise((resolve, reject) => {
      axios.get(window.url(`/projects/${params.id}`, true), { id: params.id })
        .then((response) => {
          commit(types.LOAD_PROJECT, response.data);
          resolve(response);
        })
        .catch(response => reject(response.data));
    });
  },
  /**
   * Delete an existing project
   *
   * @param commit
   * @param params
   * @returns Promise
   */
  deleteProject({ commit }, params) {
    return new Promise((resolve, reject) => {
      axios.delete(window.url(`/projects/${params.id}`, true), { params })
        .then((response) => {
          commit(types.DELETE_PROJECT, response.data);
          resolve(response);
        })
        .catch(response => reject(response.data));
    });
  },
  /**
   * Persist a new project
   *
   * @param commit
   * @param params
   * @returns Promise
   */
  createProject({ commit }, params) {
    const formData = new FormData();
    const theData = Object.entries(params);
    const requestHeaders = {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    };

    // append all the data to a FormData object
    theData.filter(entry => formData.append(entry[0], entry[1]));

    return new Promise((resolve, reject) => {
      axios.post(window.url('/projects', true), formData, requestHeaders)
        .then((response) => {
          commit(types.CREATE_PROJECT, response.data);
          resolve(response);
        })
        .catch(response => reject(response.data));
    });
  },
  /**
   * Update an existing project
   *
   * @param commit
   * @param params
   * @returns Promise
   */
  updateProject({ commit }, params) {
    const formData = new FormData();
    const theData = Object.entries(params);
    const requestHeaders = {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    };

    // append all the data to a FormData object
    // check image fields to actually be image files instead of paths
    theData.filter((entry) => {
      const inputName = entry[0];
      const inputValue = entry[1];
      if (inputName === 'image' || inputName === 'image_2') {
        if (!window.isString(inputValue)) {
          formData.append(inputName, inputValue);
        }
        return false;
      }

      formData.append(inputName, inputValue);
    });

    // set the method to PUT in order to use proper API route
    formData.append('_method', 'PUT');

    return new Promise((resolve, reject) => {
      axios.post(window.url(`/projects/${params.id}`, true), formData, requestHeaders)
        .then((response) => {
          commit(types.UPDATE_PROJECT, response.data);
          resolve(response);
        })
        .catch(response => reject(response.data));
    });
  },
};
