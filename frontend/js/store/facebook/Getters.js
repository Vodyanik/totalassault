export default {
  dailyPerformanceRecords: state => state.dailyPerformanceRecords.data,
  dailyPerformancePagination: state => state.dailyPerformanceRecords.pagination,
};
