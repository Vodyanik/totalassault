import state from 'store/facebook/State';
import actions from 'store/facebook/Actions';
import mutations from 'store/facebook/Mutations';
import getters from 'store/facebook/Getters';

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
