import * as types from 'store/facebook/MutationTypes';

export default {
  [types.LOAD_FB_DAILY_PERFORMANCE](state, response) {
    state.dailyPerformanceRecords.data = response.data;
    state.dailyPerformanceRecords.pagination.current_page = parseInt(response.meta.current_page, 10);
    state.dailyPerformanceRecords.pagination.per_page = parseInt(response.meta.per_page, 10);
    state.dailyPerformanceRecords.pagination.total_pages = parseInt(response.meta.last_page, 10);
    state.dailyPerformanceRecords.pagination.total = parseInt(response.meta.total, 10);
  },
  [types.DELETE_PROJECT](state) {
    state.project = null;
  },
  [types.CREATE_PROJECT](state, response) {
    state.project = response.data;
  },
  [types.UPDATE_PROJECT](state, response) {
    state.project = response.data;
  },
};
