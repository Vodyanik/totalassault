export default {
  projects: state => state.projects.data,
  pagination: state => state.projects.pagination,
  project: state => state.project,
};
