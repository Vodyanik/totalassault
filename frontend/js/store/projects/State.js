export default {
  projects: {
    pagination: {
      current_page: 1,
      total_pages: 1,
      per_page: 20,
      total: 20,
    },
    data: [],
  },
  project: {},
};
