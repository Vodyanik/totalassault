import * as types from 'store/projects/MutationTypes';

export default {
  [types.LOAD_PROJECTS](state, response) {
    state.projects.data = response.data;
    state.projects.pagination.current_page = parseInt(response.meta.current_page, 10);
    state.projects.pagination.per_page = parseInt(response.meta.per_page, 10);
    state.projects.pagination.total_pages = parseInt(response.meta.last_page, 10);
    state.projects.pagination.total = parseInt(response.meta.total, 10);
  },
  [types.DELETE_PROJECT](state) {
    state.project = null;
  },
  [types.CREATE_PROJECT](state, response) {
    state.project = response.data;
  },
  [types.LOAD_PROJECT](state, response) {
    state.project = response.data;
  },
  [types.UPDATE_PROJECT](state, response) {
    state.project = response.data;
  },
};
