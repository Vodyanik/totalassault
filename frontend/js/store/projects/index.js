import state from 'store/projects/State';
import actions from 'store/projects/Actions';
import mutations from 'store/projects/Mutations';
import getters from 'store/projects/Getters';

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
