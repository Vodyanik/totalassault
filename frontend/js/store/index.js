import Vuex from 'vuex';
import Vue from 'vue';
import users from 'store/users';
import campaigns from 'store/campaigns';
import projects from 'store/projects';
import facebook from 'store/facebook';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    users,
    campaigns,
    projects,
    facebook,
  },
});
