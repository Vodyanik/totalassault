// the functions for retrieving the state
export default {
  campaigns: (state) => {
    const campaigns = state.campaigns.data;
    campaigns.forEach((campaign) => {
      campaign.cost = parseFloat(campaign.cost_per_click) * parseFloat(campaign.clicks);
      campaign.budget_remaining = parseFloat(campaign.budget) - parseFloat(campaign.cost);
      const today = new Date();
      const dateToReply = new Date(campaign.end_date);
      const timeInMilisec = dateToReply.getTime() - today.getTime();
      campaign.days_remaining = Math.ceil(timeInMilisec / (1000 * 60 * 60 * 24));
    });
    return campaigns;
  },
  campaign: state => state.campaign,
};
