import * as types from 'store/campaigns/MutationTypes';

export default {
  [types.LOAD_CAMPAIGNS](state, response) {
    state.campaigns.data = response.data;
    state.campaigns.pagination.current_page = parseInt(response.meta.current_page, 10);
    state.campaigns.pagination.per_page = parseInt(response.meta.per_page, 10);
    state.campaigns.pagination.total_pages = parseInt(response.meta.last_page, 10);
    state.campaigns.pagination.total = parseInt(response.meta.total, 10);
  },
  [types.DELETE_CAMPAIGN](state) {
    state.campaign = null;
  },
  [types.CREATE_CAMPAIGN](state, response) {
    state.campaign = response.data;
  },
  [types.LOAD_CAMPAIGN](state, response) {
    state.campaign = response.data;
  },
  [types.UPDATE_CAMPAIGN](state, response) {
    state.campaign = response.data;
  },
};
