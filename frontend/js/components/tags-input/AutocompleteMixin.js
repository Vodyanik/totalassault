const AutocompleteMixin = {
  props: {
    autocomplete: {
      default: false,
      type: Boolean
    },
    url: {
      default: "",
      type: String
    },
    searchField: {
      default: "title",
      type: String
    },
    sortKey: {
      default: "id",
      type: String
    },
    sortDirection: {
      default: "ASC",
      type: String
    }
  },
  methods: {
    getSearchFilter: function (searchKeyword) {
      return {
        "or": true,
        "filters": [
          {
            "key": this.searchField,
            "value": searchKeyword,
            "operator": "ct"
          }
        ]
      };
    },
    parseData: function (data) {
      return data.map((item) => {
        return {
          value: item.id,
          text: item[this.searchField]
        };
      });
    },
    selectizeLoad: function (query, callback) {

      console.dir(query);

      if (!query.length) {
        return callback();
      }

      const url = this.apiUrl + this.url;
      const searchFilter = this.getSearchFilter(query);
      const defaultSort = { "key": this.sortKey, "direction": this.sortDirection };
      const queryParameters = {
        "limit": 20,
        "page": 0,
        "sort": [ defaultSort ],
        "filter_groups": [ searchFilter ]
      };
      const parameters = $.param(queryParameters);

      axios.get(url + '?' + parameters)
        .then((response) => {
          const data = this.parseData(response.data.data);
          callback(data);
        })
        .catch(function (error) {
          console.dir(error);
          callback();
        });
    },
    enableAutocomplete: function () {
      if (typeof this.autocomplete !== "undefined" && Boolean(this.autocomplete) === true) {
        this.settings.load = this.selectizeLoad;
      }
    },
  }
};

export default AutocompleteMixin;
