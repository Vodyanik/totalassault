import CustomizeColumns from 'components/common/CustomizeColumns';
import ExportData from 'components/common/ExportData';
import HeadPicker from 'components/common/HeadPicker';
import HeadTitle from 'components/common/HeadTitle';
import SelectCampaign from 'components/common/SelectCampaign';
import ViewTypes from 'components/common/ViewTypes';

export default {
  components: {
    CustomizeColumns,
    ExportData,
    HeadPicker,
    HeadTitle,
    SelectCampaign,
    ViewTypes,
  },
  data() {
    return {
      viewType: 'list',
      headDropdownValue: 0,
      headDropdownOptions: [],
      headDropdownValueField: 'id',
      headDropdownTextField: 'name',
      displayedFields: [],
      rangePickerOptions: {
        opens: 'left',
        startDate: moment(),
        endDate: moment().add(7, 'days'),
      },
      dateRange: {
        start: moment(),
        end: moment().add(7, 'days'),
      },
      pagination: {
        current_page: 1,
        total_pages: 1,
        per_page: 20,
        total: 20,
      },
      searchKeyword: '',
    };
  },
  mounted() {
    this.initialDateRange();
    this.initialHeadDropdownValue();
    this.setDropdownOptions([]);
    this.displayedFields = this.fields;
    this.$refs.customizeColumns.$on('customizeColumns.change', this.changeColumns);
    this.$refs.headTitle.$on('headTitle.viewType.change', this.changeViewType);
    this.$refs.headPicker.$on('headPicker.change', this.changeDateRange);
    this.$refs.headDropdown.$on('headDropdown.change', this.changeHeadDropdownValue);
  },
  methods: {
    changeColumns(displayFields) {
      this.displayedFields = displayFields;
    },
    changeViewType(viewType) {
      this.viewType = viewType;
    },
    changeDateRange(dateRange) {
      this.dateRange = dateRange;
    },
    changeHeadDropdownValue(dropdownValue) {
      this.headDropdownValue = dropdownValue;
    },
    setDropdownOptions(newOptions) {
      let options = [];
      const placeholder = {};
      placeholder[this.headDropdownValueField] = 0;
      placeholder[this.headDropdownTextField] = this.headDropdownPlaceholder;

      options.push(placeholder);
      options = options.concat(newOptions);

      this.headDropdownOptions = options;
    },
    initialDateRange() {
      const dateRange = localStorage.getItem('taraRangeFilter');

      if (dateRange !== null) {
        try {
          this.dateRange = JSON.parse(dateRange);
          this.dateRange.start = moment(this.dateRange.start);
          this.dateRange.end = moment(this.dateRange.end);

          this.rangePickerOptions.startDate = this.dateRange.start;
          this.rangePickerOptions.endDate = this.dateRange.end;
        } catch (error) {
          console.dir('There was a problem parsig the JSON in initialDateRange');
        }
      }
    },
    initialHeadDropdownValue() {
      const headDropdownValue = parseInt(localStorage.getItem('taraHeadDropdownFilter'), 10);

      if (!isNaN(headDropdownValue)) {
        this.headDropdownValue = headDropdownValue;
      }
    },
  },
  watch: {
    /**
     * Store the dateRange in the localStorage for later reuse
     */
    dateRange: {
      handler(newValue) {
        const { dateRange } = this;

        dateRange.start = moment(newValue.start).format('YYYY-MM-DD');
        dateRange.end = moment(newValue.end).format('YYYY-MM-DD');

        if (newValue.start !== moment().format('YYYY-MM-DD') ||
          newValue.end !== moment().add(7, 'days').format('YYYY-MM-DD')
        ) {
          // save filter to local storage
          localStorage.setItem('taraRangeFilter', JSON.stringify(dateRange));
        }
      },
      deep: true,
    },
    /**
     * Save the headDropdownValue in the localStorage for later reuse
     *
     * @param dropdownValue
     */
    headDropdownValue(dropdownValue) {
      const parsedValue = parseInt(dropdownValue, 10);

      if (!isNaN(parsedValue)) {
        localStorage.setItem('taraHeadDropdownFilter', parsedValue);
      }
    },
  },
};
