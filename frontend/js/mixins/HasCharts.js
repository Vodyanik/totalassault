import BarChart from 'components/charts/BarChart';
import LineChart from 'components/charts/LineChart';

export default {
  components: {
    BarChart,
    LineChart,
  },
};
