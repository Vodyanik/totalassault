export default {
  data() {
    return {
      queryObject: {},
    };
  },
  watch: {
    queryObject: {
      deep: true,
      handler() {
        this.loadRecords();
      },
    },
  },
  mounted() {
    this.$refs.pagination.$on('input', this.updateQueryObject);
    this.$refs.headPicker.$on('headPicker.change', this.updateQueryObject);
    this.$refs.headDropdown.$on('headDropdown.change', this.updateQueryObject);
  },
  methods: {
    generateQueryObject() {
      const queryObject = {};

      // limit & page
      queryObject.limit = this.pagination.per_page;
      queryObject.page = this.pagination.current_page;

      // create date range filter
      const filters = [this.getRangeFilter(), this.getHeadDropdownFilter()];

      // create filters_group
      queryObject.filter_groups = [];
      queryObject.filter_groups.push(window.createFilterGroup(filters));

      return queryObject;
    },
    updateQueryObject() {
      this.queryObject = this.generateQueryObject();
    },
    getRangeFilter() {
      const storageFilter = localStorage.getItem('taraRangeFilter');
      let dateRange;

      try {
        dateRange = storageFilter !== null ? JSON.parse(storageFilter) : this.dateRange;
      } catch (error) {
        dateRange = this.dateRange;
      }

      dateRange.start = moment(dateRange.start).format('YYYY-MM-DD');
      dateRange.end = moment(dateRange.end).format('YYYY-MM-DD');

      return window.createFilter('created_at', 'bt', [dateRange.start, dateRange.end]);
    },
    getHeadDropdownFilter() {
      const storageFilter = localStorage.getItem('taraHeadDropdownFilter');
      let { headDropdownValue } = this;

      if (this.headDropdownValue === 0 && storageFilter !== null) {
        try {
          headDropdownValue = JSON.parse(storageFilter);
        } catch (error) {
          headDropdownValue = this.headDropdownValue;
          console.dir('There was an error while parsing JSON in getHeadDropdownFilter()');
        }
      }

      // don't set any filter if the dropdown value is 0
      if (headDropdownValue === 0) {
        return {};
      }

      return window.createFilter('campaign_id', 'eq', headDropdownValue);
    },
  },
};
