import Vue from 'vue';

const general = {
  // don't use any global data variable
  data() { return {}; },
  // don't use, it triggers on every single component
  mounted() {},
  methods: {
    validate(successCallback) {
      this.$validator.validateAll().then((result) => {
        if (!result) {
          // fail validation
          $('html,body').animate({ scrollTop: 0 }, 'slow');
          return this.fail('main.invalidRequest');
        }

        // success validation
        if (typeof successCallback === 'function') {
          return successCallback();
        }

        return null;
      });
    },
    isset(variable, defaultValue = null) {
      return typeof variable !== 'undefined' ? variable : defaultValue;
    },
    imageUrl(image) {
      return this.fileUrl('', image);
    },
    fileUrl(directory, file) {
      return `${this.baseUrl} /storage${directory}/${file}`;
    },
    redirectTo(url) {
      this.$router.replace(url);
    },
    reloadPage() {
      this.$router.go(this.$router.currentRoute);
    },
    youSure(message, danger = false) {
      return swal({
        title: this.$t('main.youSure'),
        text: this.$t(message),
        icon: 'warning',
        buttons: true,
        dangerMode: danger,
      });
    },
    success(message) {
      return swal(this.$t('main.goodJob'), this.$t(message), 'success');
    },
    fail(message) {
      return swal(this.$t('main.oups'), this.$t(message), 'error');
    },
    lang(message) {
      return this.$t(message);
    },
    destroyAlert(id, methodName, message) {
      this.youSure(message, true)
        .then((willDelete) => {
          if (willDelete) {
            this[methodName](id);
          }
        });
    },
    dateFormat(date) {
      return moment(date.date).format('MMMM DD YYYY, h:mm');
    },
    dateFormatStd(date) {
      return window.dateFormatStd(date);
    },
    searchRecords() {
      // create search filter
      const searchFilter = this.simpleSearchFilter();
      const filters = [searchFilter];

      // retrieve records
      this.getRecords(20, 0, filters);
    },
    simpleSearchFilter(field = 'title') {
      // don't apply any filters if the search keyword is empty
      if (this.searchKeyword.trim().length === 0) {
        return {};
      }

      return {
        or: true,
        filters: [
          {
            key: field,
            value: this.searchKeyword,
            operator: 'ct',
          },
        ],
      };
    },
    getRecords(limit = 20, page = 0, filterGroups = [], sortKey = 'id', sortDirection = 'DESC') {
      const url = this.apiUrl + this.route;
      const defaultSort = { key: sortKey, direction: sortDirection };
      const queryParameters = {
        limit,
        page,
        sort: [defaultSort],
        filter_groups: filterGroups,
      };
      const parameters = $.param(queryParameters);

      axios.get(`${url}?${parameters}`)
        .then((response) => {
          const info = response.data;

          this.records = info.data;
          this.pagination = info.meta.pagination;
          this.ready = true;

          window.scrollTo(0, 0);
        })
        .catch(error => console.dir(error));
    },
    componentExists(componentName) {
      return typeof Vue.options.components[componentName] !== 'undefined';
    },
    objectToArray(object) {
      const entries = Object.entries(object);
      return entries.map(entry => entry[1]);
    },
    activeLink: function (link, activeClass) {
      if (link === this.$route.path) {
        return activeClass;
      }
      return '';
    },
    isString(variable) {
      return typeof variable === 'string';
    },
  },
};

Vue.mixin(general);
