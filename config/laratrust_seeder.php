<?php

return [
    'role_structure' => [
        'admin' => [
            'project' => 'c,r,u,d',
        ],
        'developer' => [
            'project' => 'c,r,u,d',
        ],
        'client' => [
            'project' => 'c,r',
        ],
    ],
    'permission_structure' => [
        'cru_user' => [

        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
