<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Dev\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();
    }

    /**
     * Redirect all vue routes to the app.blade view.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::post('/api/v1/auth', 'App\Dev\Controllers\Auth\LoginController@login');
        // redirect vue routes
        Route::get('/{vue?}', function () {
            return view('app');
        })->where('vue', '^(?!.*api).*$[\/\w\.-]*');
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('/api/v1')
             ->middleware(['api', 'auth:api'])
             ->namespace($this->namespace)
             ->group(base_path('app/Dev/routes.php'));
    }
}
