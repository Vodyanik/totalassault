<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ContractServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Bind Interfaces to actual Implementations
        // This is good because it makes testing easier (Mocking)
        $services = $this->servicesBindings();
        $repositories = $this->repositoriesBindings();
        $array_to_bind = array_merge($services, $repositories);

        $this->bindArrayToApp($array_to_bind);
    }


    /**
     * Bind the service contracts to implementations
     *
     * @return array
     */
    protected function servicesBindings(): array
    {
        return [
            'Services\CampaignService',
            'Services\ProjectService',
            'Services\FbDailyPerformanceService',
            'Services\FbVideoPerformanceService',
            'Services\UserService',
            'Services\CompanyService',
            'Services\FbPageService',
            'Services\FbCanvasPerformanceService',
            'Services\CsvService',
        ];
    }

    /**
     * Bind the repository contracts to implementations
     *
     * @return array
     */
    protected function repositoriesBindings() : array
    {
        return [
            'Repositories\CampaignRepository',
            'Repositories\ProjectRepository',
            'Repositories\FbDailyPerformanceRepository',
            'Repositories\FbVideoPerformanceRepository',
            'Repositories\UserRepository',
            'Repositories\CompanyRepository',
            'Repositories\FbPageRepository',
            'Repositories\FbCanvasPerformanceRepository',
        ];
    }

    /**
     * Enable Array to App binding
     *
     * @param $array
     */
    protected function bindArrayToApp($array)
    {
        array_map(function ($item) {
            $this->app->bind("App\Dev\Contracts\\$item", "App\Dev\\$item");
        }, $array);
    }
}
