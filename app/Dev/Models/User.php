<?php

namespace App\Dev\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, LaratrustUserTrait;

    const IMG_DIR = DS . 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'company_id',
        'email',
        'password',
        'office_phone',
        'mobile_phone',
        'job_title',
        'address_1',
        'address_2',
        'country_id',
        'city_id',
        'state_id',
        'zip',
        'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
