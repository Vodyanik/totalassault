<?php

namespace App\Dev\Models\Facebook;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = "fb_pages";

    protected $fillable = [
        'name',
        'username',
        'link',
        'category_id',
        'access_token',
        'auth_user_id',
        'auth_user_name',
        'auth_user_token',
        'status',
    ];
}
