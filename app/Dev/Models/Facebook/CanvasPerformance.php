<?php

namespace App\Dev\Models\Facebook;

use Illuminate\Database\Eloquent\Model;

class CanvasPerformance extends Model
{
    protected $table = "fb_canvas_performance";

    protected $fillable = [
        'campaign_id',
        'cost',
        'impressions',
        'cpm',
        'actions',
        'cpa',
        'canvas_opens'
    ];
}
