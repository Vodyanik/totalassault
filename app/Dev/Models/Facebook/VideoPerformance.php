<?php

namespace App\Dev\Models\Facebook;

use Illuminate\Database\Eloquent\Model;

class VideoPerformance extends Model
{
    protected $table = "fb_video_performance";

    protected $fillable = [
        'campaign_id',
        'cost',
        'impressions',
        'video_views_3_seconds',
        'video_views_10_seconds',
        'video_views_30_seconds',
        'video_views_25_percents',
        'video_views_50_percents',
        'video_views_75_percents',
        'video_views_100_percents',
        'avg_time_watched',
    ];
}
