<?php

namespace App\Dev\Models\Facebook;

use Illuminate\Database\Eloquent\Model;

class DailyPerformance extends Model
{
    protected $table = "fb_daily_performance";

    protected $fillable = [
        'ad_id',
        'adset_id',
        'user_id',
        'campaign_id',
        'date_start',
        'date_stop',
        'spend',
        'purchases',
        'cpa',
        'conv_value',
        'net',
        'roas',
        'clicks',
        'cpc',
        'impressions',
        'checkout_int',
    ];
}
