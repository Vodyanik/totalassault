<?php

namespace App\Dev\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    const IMG_DIR = DS . 'projects';

    protected $fillable = [
        'name',
        'image',
        'image_2',
        'facebook_id',
        'twitter_id',
        'instagram_id',
        'youtube_id',
        'soundcloud_id',
        'publicity_status',
        'publicity_bc_team_id',
        'assets',
        'status'
    ];
}
