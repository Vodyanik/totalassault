<?php

namespace App\Dev\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'name',
        'status',
        'report',
        'user_id',
        'purchases',
        'clicks',
        'impressions',
        'checkout_init',
        'budget',
        'spend',
        'remaining',
        'cpa',
        'conversion_value',
        'nev',
        'roas',
        'cost_per_click',
        'start_date',
        'end_date'
    ];
}
