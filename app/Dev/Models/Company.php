<?php

namespace App\Dev\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    const IMG_DIR = DS . 'companies';

    protected $fillable = [
        'name',
        'image',
        'email',
        'phone',
        'address_1',
        'address_2',
        'city_id',
        'state_id',
        'country_id',
        'zip',
        'timezone',
        'currency',
        'updated_at',
        'created_at',
    ];
}
