<?php

declare(strict_types = 1);

namespace App\Dev\Controllers;

use App\Abstractions\Controller;
use App\Dev\Contracts\Services\CampaignService as Service;
use App\Dev\Requests\Campaign\Store;
use App\Dev\Requests\Campaign\Update;
use App\Dev\Requests\Campaign\Destroy;
use App\Dev\Resources\CampaignResource as Resource;
use Illuminate\Http\JsonResponse;

class CampaignController extends Controller
{
    /**
     * CampaignController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List the Campaigns
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $campaigns = $this->service->get();
        return (Resource::collection($campaigns))->response();
    }

    /**
     * Show a Campaign
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $campaign = $this->service->show($id);
        return (new Resource($campaign))->response();
    }

    /**
     * Persist a Campaign
     *
     * @param Store $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $campaign = $this->service->store($request);
        return (new Resource($campaign))->response();
    }

    /**
     * Update an existing Campaign
     *
     * @param Update $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $campaign = $this->service->update($request, $id);
        return (new Resource($campaign))->response();
    }

    /**
     * Remove a Campaign
     *
     * @param Destroy $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $campaign = $this->service->destroy($id);
        return (new Resource($campaign))->response();
    }
}
