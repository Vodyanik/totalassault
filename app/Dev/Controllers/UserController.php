<?php

declare(strict_types = 1);

namespace App\Dev\Controllers;

use App\Abstractions\Controller;
use Illuminate\Http\JsonResponse;
use App\Dev\Contracts\Services\UserService as Service;
use App\Dev\Resources\UserResource as Resource;
use App\Dev\Requests\User\Store;
use App\Dev\Requests\User\Update;
use App\Dev\Requests\User\Destroy;

class UserController extends Controller
{
    /**
     * UserController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List the Users
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $users = $this->service->get();
        return (Resource::collection($users))->response();
    }

    /**
     * Show a User
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $user = $this->service->show($id);
        return (new Resource($user))->response();
    }

    /**
     * Persist a User
     *
     * @param Store $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $user = $this->service->store($request);
        return (new Resource($user))->response();
    }

    /**
     * Update an existing User
     *
     * @param Update $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $user = $this->service->update($request, $id);
        return (new Resource($user))->response();
    }

    /**
     * Remove a User
     *
     * @param Destroy $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $campaign = $this->service->destroy($id);
        return (new Resource($campaign))->response();
    }
}
