<?php

declare(strict_types = 1);

namespace App\Dev\Controllers\Auth;

use App\Abstractions\Controller;
use App\Dev\Requests\LoginRequest;

class LoginController extends Controller
{
    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    public function login(LoginRequest $request)
    {
        try {
            $action = $this->loginProxy->attemptLogin($request->email, $request->password);
            if (!$action) {
                throw new \Exception("Failed login");
            }
        } catch (\Throwable $e) {
            return response('Failed login', 401);
        }

        return response($action);
    }
}
