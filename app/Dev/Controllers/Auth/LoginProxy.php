<?php

declare(strict_types = 1);

namespace App\Dev\Controllers\Auth;

use App\Dev\Repositories\UserRepository;
use Illuminate\Foundation\Application;
use App\Dev\Resources\UserResource as Resource;

class LoginProxy
{
    const REFRESH_TOKEN = 'refreshToken';
    private $apiConsumer;
    private $auth;
    private $cookie;
    private $db;
    private $request;
    private $userRepository;
    private $user;

    public function __construct(Application $app, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->apiConsumer = $app->make('apiconsumer');
        $this->auth = $app->make('auth');
        $this->cookie = $app->make('cookie');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }

    public function attemptLogin($email, $password)
    {
        $this->user = $this->userRepository->findWhereFirst('email', $email);

        if (!is_null($this->user)) {
            return $this->proxy('password', [
                'username' => $email,
                'password' => $password,
            ]);
        }

        return false;
    }

    public function proxy($grantType, array $data = [])
    {
        $data = array_merge($data, [
            'client_id'     => env('PASSPORT_CLIENT_ID'),
            'client_secret' => env('PASSPORT_CLIENT_SECRET'),
            'grant_type'    => $grantType
        ]);

        $response = $this->apiConsumer->post('/oauth/token', $data);

        if (!$response->isSuccessful()) {
            return false;
        }

        $data = json_decode($response->getContent());

        // Create a refresh token cookie
        $this->cookie->queue(
            self::REFRESH_TOKEN,
            $data->refresh_token,
            864000, // 10 days
            null,
            null,
            false,
            true // HttpOnly
        );

        return [
            'access_token' => $data->access_token,
            'expires_in' => $data->expires_in,
            'user_data' => (new Resource($this->user)),
        ];
    }
}
