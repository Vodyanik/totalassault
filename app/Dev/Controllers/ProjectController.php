<?php

declare(strict_types = 1);

namespace App\Dev\Controllers;

use App\Abstractions\Controller;
use App\Dev\Contracts\Services\ProjectService as Service;
use App\Dev\Requests\Project\Show;
use App\Dev\Requests\Project\Store;
use App\Dev\Requests\Project\Update;
use App\Dev\Requests\Project\Destroy;
use App\Dev\Resources\ProjectResource as Resource;
use Illuminate\Http\JsonResponse;

class ProjectController extends Controller
{
    /**
     * ProjectController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List the Projects
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Show $request): JsonResponse
    {
        $campaigns = $this->service->get();
        return (Resource::collection($campaigns))->response();
    }

    /**
     * Show a Project
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Show $request, string $id): JsonResponse
    {
        $campaign = $this->service->show($id);
        return (new Resource($campaign))->response();
    }

    /**
     * Persist a Project
     *
     * @param Store $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $campaign = $this->service->store($request);
        return (new Resource($campaign))->response();
    }

    /**
     * Update an existing Project
     *
     * @param Update $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $campaign = $this->service->update($request, $id);
        return (new Resource($campaign))->response();
    }

    /**
     * Remove a Project
     *
     * @param Destroy $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $campaign = $this->service->destroy($id);
        return (new Resource($campaign))->response();
    }
}
