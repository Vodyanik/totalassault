<?php

namespace App\Dev\Controllers\Facebook;

use App\Abstractions\Controller;
use App\Dev\Contracts\Services\FbVideoPerformanceService as Service;
use App\Dev\Resources\FbVideoPerformanceResource as Resource;
use Illuminate\Http\JsonResponse;
use App\Dev\Requests\FbVideoPerformance\Store;
use App\Dev\Requests\FbVideoPerformance\Update;
use App\Dev\Requests\FbVideoPerformance\Destroy;

class VideoPerformanceController extends Controller
{
    /**
     * FacebookVideoAdPerformanceController constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List Facebook video ad performances.
     *
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
        $videoPerformance = $this->service->get();
        return (Resource::collection($videoPerformance))->response();
    }

    /**
     * Show Facebook video ad performance.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id) : JsonResponse
    {
        $videoPerformance = $this->service->show($id);
        return (new Resource($videoPerformance))->response();
    }

    /**
     * Store Facebook video ad performance.
     *
     * @param Store $request
     * @return JsonResponse
     */
    public function store(Store $request) : JsonResponse
    {
        $videoPerformance = $this->service->store($request);
        return (new Resource($videoPerformance))->response();
    }

    /**
     * Update Facebook video ad performance.
     *
     * @param Update $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(Update $request, string $id) : JsonResponse
    {
        $videoPerformance = $this->service->update($request, $id);
        return (new Resource($videoPerformance))->response();
    }

    /**
     * Destroy Facebook video ad performance.
     *
     * @param Destroy $request
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(Destroy $request, string $id) : JsonResponse
    {
        $videoPerformance = $this->service->destroy($id);
        return (new Resource($videoPerformance))->response();
    }
}
