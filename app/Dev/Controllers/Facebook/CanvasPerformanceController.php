<?php

namespace App\Dev\Controllers\Facebook;

use App\Abstractions\Controller;
use App\Dev\Contracts\Services\FbCanvasPerformanceService as Service;
use App\Dev\Resources\FbCanvasPerformanceResource as Resource;
use Illuminate\Http\JsonResponse;
use App\Dev\Requests\FbCanvasPerformance\Store;
use App\Dev\Requests\FbCanvasPerformance\Update;
use App\Dev\Requests\FbCanvasPerformance\Destroy;

class CanvasPerformanceController extends Controller
{
    /**
     * FacebookVideoAdPerformanceController constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List Facebook canvas ad performances.
     *
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
        $facebook_canvas_performance = $this->service->get();
        return (Resource::collection($facebook_canvas_performance))->response();
    }

    /**
     * Show Facebook canvas ad performance.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id) : JsonResponse
    {
        $facebook_canvas_performance = $this->service->show($id);
        return (new Resource($facebook_canvas_performance))->response();
    }

    /**
     * Store Facebook canvas ad performance.
     *
     * @param Store $request
     * @return JsonResponse
     */
    public function store(Store $request) : JsonResponse
    {
        $facebook_canvas_performance = $this->service->store($request);
        return (new Resource($facebook_canvas_performance))->response();
    }

    /**
     * Update Facebook canvas ad performance.
     *
     * @param Update $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(Update $request, string $id) : JsonResponse
    {
        $facebook_canvas_performance = $this->service->update($request, $id);
        return (new Resource($facebook_canvas_performance))->response();
    }

    /**
     * Destroy Facebook canvas ad performance.
     *
     * @param Destroy $request
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(Destroy $request, string $id) : JsonResponse
    {
        $facebook_canvas_performance = $this->service->destroy($id);
        return (new Resource($facebook_canvas_performance))->response();
    }
}
