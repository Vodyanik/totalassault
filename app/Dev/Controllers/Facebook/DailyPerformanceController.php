<?php

declare(strict_types = 1);

namespace App\Dev\Controllers\Facebook;

use App\Abstractions\Controller;
use App\Dev\Contracts\Repositories\FbDailyPerformanceRepository;
use App\Dev\Requests\FbDailyPerformance\ExportCsv;
use App\Dev\Contracts\Services\CsvService;
use Illuminate\Http\JsonResponse;
use App\Dev\Requests\FbDailyPerformance\Store;
use App\Dev\Requests\FbDailyPerformance\Update;
use App\Dev\Requests\FbDailyPerformance\Destroy;
use App\Dev\Contracts\Services\FbDailyPerformanceService as Service;
use App\Dev\Resources\FbDailyPerformanceResource as Resource;

class DailyPerformanceController extends Controller
{
    protected $csvService;

    /**
     * DailyPerformanceController constructor.
     * @param Service $service
     * @param CsvService $csvService
     */
    public function __construct(
        Service $service,
        CsvService $csvService
    ) {
        $this->service = $service;
        $this->csvService = $csvService;
    }

    /**
     * List the DailyPerformances
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $dailyPerformance = $this->service->get();
        return (Resource::collection($dailyPerformance))->response();
    }

    /**
     * Store a DailyPerformance
     *
     * @param Store $request
     * @return JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $dailyPerformance = $this->service->store($request);
        return (new Resource($dailyPerformance))->response();
    }

    /**
     * Show a DailyPerformance
     *
     * @param $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $dailyPerformance = $this->service->show($id);
        return (new Resource($dailyPerformance))->response();
    }

    /**
     * Update an existing DailyPerformance
     *
     * @param Update $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $dailyPerformance = $this->service->update($request, $id);
        return (new Resource($dailyPerformance))->response();
    }

    /**
     * Remove a DailyPerformance
     *
     * @param $request
     * @param $id
     * @return JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $dailyPerformance = $this->service->destroy($id);
        return (new Resource($dailyPerformance))->response();
    }

    /**
     * Export a csv file containing daily performance data
     *
     * @param ExportCsv $request
     * @param FbDailyPerformanceRepository $repository
     * @return \Illuminate\Http\Response
     */
    public function exportCsv(ExportCsv $request, FbDailyPerformanceRepository $repository)
    {
        return $this->csvService->export($request, $repository);
    }
}
