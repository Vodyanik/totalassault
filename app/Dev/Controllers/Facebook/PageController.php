<?php
declare(strict_types = 1);

namespace App\Dev\Controllers\Facebook;

use App\Abstractions\Controller;
use Illuminate\Http\JsonResponse;
use App\Dev\Requests\FbPage\Store;
use App\Dev\Requests\FbPage\Update;
use App\Dev\Requests\FbPage\Destroy;
use App\Dev\Contracts\Services\FbPageService as Service;
use App\Dev\Resources\FbPageResource as Resource;

class PageController extends Controller
{
    /**
     * FacebookController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }
    /**
     * List Facebook page
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $facebook_page = $this->service->get();
        return (Resource::collection($facebook_page))->response();
    }
    /**
     * Store Facebook page
     *
     * @param Store $request
     * @return JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $facebook_page = $this->service->store($request);
        return (new Resource($facebook_page))->response();
    }
    /**
     * Show Facebook page
     *
     * @param $hash_id
     * @return JsonResponse
     */
    public function show(string $hash_id): JsonResponse
    {
        $facebook_page = $this->service->show($hash_id);
        return (new Resource($facebook_page))->response();
    }
    /**
     * Update existing Facebook page
     *
     * @param Update $request
     * @param $hash_id
     * @return JsonResponse
     */
    public function update(Update $request, $hash_id): JsonResponse
    {
        $facebook_page = $this->service->update($request, $hash_id);
        return (new Resource($facebook_page))->response();
    }
    /**
     * Remove Facebook page
     *
     * @param $request
     * @param $hash_id
     * @return JsonResponse
     */
    public function destroy(Destroy $request, string $hash_id): JsonResponse
    {
        $facebook_page = $this->service->destroy($hash_id);
        return (new Resource($facebook_page))->response();
    }
}
