<?php

declare(strict_types = 1);

namespace App\Dev\Controllers;

use App\Abstractions\Controller;
use App\Dev\Contracts\Services\CompanyService as Service;
use App\Dev\Requests\Company\Store;
use App\Dev\Requests\Company\Update;
use App\Dev\Requests\Company\Destroy;
use App\Dev\Resources\CompanyResource as Resource;
use Illuminate\Http\JsonResponse;

class CompanyController extends Controller
{
    /**
     * CompanyController constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List the Companies
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $companies = $this->service->get();
        return (Resource::collection($companies))->response();
    }

    /**
     * Show a Company
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $company = $this->service->show($id);
        return (new Resource($company))->response();
    }

    /**
     * Persist a Company
     *
     * @param Store $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $company = $this->service->store($request);
        return (new Resource($company))->response();
    }

    /**
     * Update an existing Company
     *
     * @param Update $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $company = $this->service->update($request, $id);
        return (new Resource($company))->response();
    }

    /**
     * Remove a Company
     *
     * @param Destroy $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $company = $this->service->destroy($id);
        return (new Resource($company))->response();
    }
}
