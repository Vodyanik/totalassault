<?php

namespace App\Dev\Services;

use App\Abstractions\Service;
use App\Dev\Contracts\Services\FbVideoPerformanceService as ServiceContract;
use App\Dev\Contracts\Repositories\FbVideoPerformanceRepository as Repository;

class FbVideoPerformanceService extends Service implements ServiceContract
{
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }
}
