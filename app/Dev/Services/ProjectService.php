<?php

declare(strict_types = 1);

namespace App\Dev\Services;

use App\Dev\Models\Project;
use App\Dev\Contracts\Services\ProjectService as ServiceContract;
use App\Dev\Contracts\Repositories\ProjectRepository as Repository;
use App\Abstractions\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectService extends Service implements ServiceContract
{
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        // store the image file
        $data['image'] = $request->file('image')->store(Project::IMG_DIR);
        $data['image'] = basename($data['image']);

        // store the image_2 file
        if (isset($request->image_2)) {
            $data['image_2'] = $request->file('image_2')->store(Project::IMG_DIR);
            $data['image_2'] = basename($data['image_2']);
        }

        return $this->repository->create($data);
    }

    public function update(Request $request, string $id)
    {
        $project = Project::find($id);
        $data = $request->all();

        $this->processUpdateImage('image', $project, $data);
        $this->processUpdateImage('image_2', $project, $data);

        return $this->repository->update($id, $data);
    }

    public function destroy(string $id)
    {
        // delete the image files
        $project = Project::find($id);
        Storage::delete(Project::IMG_DIR . DS . $project->image);
        Storage::delete(Project::IMG_DIR . DS . $project->image_2);

        // delete the record from the database
        return parent::destroy($id);
    }

    private function processUpdateImage(string $fieldName, Project $project, array &$data)
    {
        $allowedFields = ['image', 'image_2'];
        if (!in_array($fieldName, $allowedFields)) {
            return false;
        }

        if (isset(request()->$fieldName)) {
            $data["$fieldName"] = request()->file($fieldName)->store(Project::IMG_DIR);
            $data["$fieldName"] = basename($data["$fieldName"]);

            Storage::delete(Project::IMG_DIR . DS . $project->$fieldName);
        }
    }
}
