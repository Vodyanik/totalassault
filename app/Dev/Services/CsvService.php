<?php

declare(strict_types = 1);

namespace App\Dev\Services;

use App\Abstractions\Contracts\Repository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Dev\Contracts\Services\CsvService as Contract;
use Symfony\Component\HttpFoundation\Response;

class CsvService implements Contract
{
    public function export(Request $request, Repository $repository) : Response
    {
        $records = $this->getRecords($request->all(), $repository);

        return response()
            ->download($this->createFile($records))
            ->deleteFileAfterSend(true);
    }

    public function getRecords(array $data, Repository $repository)
    {
        // apply date filters
        if (isset($data['date']) && is_array($data['date'])) {
            $date = $data['date'];
            $start_date = format_date_for_mysql($date[0]);
            $end_date = format_date_for_mysql($date[1]);

            $repository = $repository->whereBetween('created_at', [$start_date, $end_date]);
        }

        // select the columns
        if (isset($data['columns']) && is_array($data['columns'])) {
            $repository = $repository->select($data['columns']);
        }

        // paginate the date
        if (!isset($data['limit']) || isset($data['limit']) && $data['limit'] === 'null') {
            return $repository->get();
        }

        return $repository->paginate(isset($data['limit']) ? (int)$data['limit'] : 20);
    }

    public function createFile($records) : string
    {
        $file_name = $this->createFileName($records);
        $this->addDataToFile($file_name, $records);

        return base_path($file_name);
    }

    protected function addDataToFile(string $file_name, $records)
    {
        $file_handle = fopen($file_name, 'w');

        // add column names to file
        $first_record = $records[0];
        $columns = array_keys($first_record->getAttributes());
        fputcsv($file_handle, $columns);

        // add the actual data to file
        foreach ($records as $record) {
            fputcsv($file_handle, $record->toArray());
        }

        fclose($file_handle);
    }

    protected function createFileName($records) : string
    {
        $table_name = $records->first()->getTable();
        $current_date = Carbon::today()->toDateString();
        $random_string = str_random();

        return "public/" . str_slug("$table_name $current_date $random_string") . ".csv";
    }
}
