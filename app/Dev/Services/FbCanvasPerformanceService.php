<?php

namespace App\Dev\Services;

use App\Abstractions\Service;
use App\Dev\Contracts\Services\FbCanvasPerformanceService as ServiceContract;
use App\Dev\Contracts\Repositories\FbCanvasPerformanceRepository as Repository;

class FbCanvasPerformanceService extends Service implements ServiceContract
{
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }
}
