<?php

declare(strict_types = 1);

namespace App\Dev\Services;

use App\Abstractions\Service;
use App\Dev\Contracts\Services\CampaignService as ServiceContract;
use App\Dev\Contracts\Repositories\CampaignRepository as Repository;

class CampaignService extends Service implements ServiceContract
{
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }
}
