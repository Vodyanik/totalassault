<?php

declare(strict_types = 1);

namespace App\Dev\Services;

use App\Abstractions\Service;
use App\Dev\Contracts\Services\FbDailyPerformanceService as ServiceContract;
use App\Dev\Contracts\Repositories\FbDailyPerformanceRepository as Repository;

class FbDailyPerformanceService extends Service implements ServiceContract
{
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }
}
