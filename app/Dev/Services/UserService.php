<?php

declare(strict_types = 1);

namespace App\Dev\Services;

use App\Abstractions\Service;
use App\Dev\Contracts\Services\UserService as ServiceContract;
use App\Dev\Contracts\Repositories\UserRepository as Repository;
use App\Dev\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserService extends Service implements ServiceContract
{
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if (isset($request->image)) {
            $data['image'] = $request->file('image')->store(User::IMG_DIR);
            $data['image'] = basename($data['image']);
        }

        return $this->repository->create($data);
    }

    public function update(Request $request, string $id)
    {
        $project = User::find($id);
        $data = $request->all();

        if (isset($request->image)) {
            $data['image'] = $request->file('image')->store(User::IMG_DIR);
            $data['image'] = basename($data['image']);

            Storage::delete(User::IMG_DIR . DS . $project->image);
        }

        return $this->repository->update($id, $data);
    }

    public function destroy(string $id)
    {
        // delete the image files
        $project = User::find($id);
        Storage::delete(User::IMG_DIR . DS . $project->image);

        // delete the record from the database
        return parent::destroy($id);
    }
}
