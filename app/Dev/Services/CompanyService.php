<?php

declare(strict_types = 1);

namespace App\Dev\Services;

use App\Dev\Models\Company;
use App\Dev\Contracts\Services\CompanyService as ServiceContract;
use App\Dev\Contracts\Repositories\CompanyRepository as Repository;
use App\Abstractions\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyService extends Service implements ServiceContract
{
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        // store the image file
        $data['image'] = $request->file('image')->store(Company::IMG_DIR);
        $data['image'] = basename($data['image']);

        return $this->repository->create($data);
    }

    public function update(Request $request, string $id)
    {
        $company = Company::find($id);
        $data = $request->all();

        if (isset($request->image)) {
            $data["image"] = request()->file('image')->store(Company::IMG_DIR);
            $data["image"] = basename($data["image"]);

            Storage::delete(Company::IMG_DIR . DS . $company->image);
        }

        return $this->repository->update($id, $data);
    }

    public function destroy(string $id)
    {
        // delete the image file
        $company = Company::find($id);
        Storage::delete(Company::IMG_DIR . DS . $company->image);

        // delete the record from the database
        return parent::destroy($id);
    }
}
