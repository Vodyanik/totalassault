<?php

declare(strict_types = 1);

namespace App\Dev\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        // TODO: Update these rules to include required where it is needed
        return [
            'id' => 'required|integer|exists:users,id',
            'first_name' => 'string',
            'last_name' => 'string',
            'company_id' => 'integer',
            'email' => 'required|unique:users,email',
            'office_phone' => 'string', // need will be add validation for phone number
            'mobile_phone' => 'string', // need will be add validation for phone number
            'job_title' => 'string',
            'address_1' => 'string',
            'address_2' => 'string',
            'country_id' => 'integer',
            'city_id' => 'integer',
            'state_id' => 'integer',
            'zip' => 'string',
            'image' => 'file|image',
        ];
    }
}
