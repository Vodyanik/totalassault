<?php

declare(strict_types = 1);

namespace App\Dev\Requests\FbCanvasPerformance;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'id' => 'required|integer|exists:fb_canvas_performance,id',
            'campaign_id' => 'required|integer|exists:campaigns,id',
            'cost' => 'numeric',
            'impressions' => 'integer',
            'cpm' => 'numeric',
            'actions' => 'integer',
            'cpa' => 'numeric',
            'canvas_opens' => 'integer',
        ];
    }
}
