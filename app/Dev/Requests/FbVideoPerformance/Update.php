<?php

declare(strict_types = 1);

namespace App\Dev\Requests\FbVideoPerformance;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'id' => 'required|integer|exists:fb_video_performance,id',
            'campaign_id' => 'required|integer|exists:campaigns,id',
            'cost' => 'numeric',
            'impressions' => 'numeric',
            'video_views_3_seconds' => 'numeric|min:0',
            'video_views_10_seconds' => 'numeric|min:0',
            'video_views_30_seconds' => 'numeric|min:0',
            'video_views_25_percents' => 'numeric|min:0',
            'video_views_50_percents' => 'numeric|min:0',
            'video_views_75_percents' => 'numeric|min:0',
            'video_views_100_percents' => 'numeric|min:0',
            'avg_time_watched' => 'numeric|min:0',
        ];
    }
}
