<?php

declare(strict_types = 1);

namespace App\Dev\Requests\FbDailyPerformance;

use Illuminate\Foundation\Http\FormRequest;

class ExportCsv extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'date' => 'array',// for the date
            'columns' => 'array',// for the selected columns
            'limit' => 'required',
            'page' => 'integer'
        ];
    }
}
