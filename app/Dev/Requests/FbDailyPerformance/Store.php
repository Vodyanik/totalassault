<?php

declare(strict_types = 1);

namespace App\Dev\Requests\FbDailyPerformance;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        // TODO: need add check exist field with id or not
        return [
            'ad_id' => 'integer', // need create table
            'adset_id' => 'integer', // need create table
            'user_id' => 'integer', // need create table
            'campaign_id' => 'integer', // need create table
            'date_start' => 'date',
            'date_stop' => 'date',
            'spend' => 'numeric',
            'purchases' => 'integer',
            'cpa' => 'numeric',
            'conv_value' => 'numeric',
            'net' => 'numeric',
            'roas' => 'numeric',
            'clicks' => 'integer',
            'cpc' => 'numeric',
            'impressions' => 'integer',
            'checkout_int' => 'integer',
        ];
    }
}
