<?php

namespace App\Dev\Requests\FbDailyPerformance;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // TODO: need add check exist field with id or not
        return [
            'id' => 'required|integer|exists:fb_daily_performance,id',
            'ad_id' => 'integer', // need create table
            'user_id' => 'integer', // need create table
            'account_id' => 'integer', // need create table
            'campaign_id' => 'integer', // need create table
            'date_start' => 'required|date',
            'date_stop' => 'required|date',
            'spend' => 'numeric',
            'purchases' => 'integer',
            'cpa' => 'numeric',
            'conv_value' => 'numeric',
            'net' => 'numeric',
            'roas' => 'numeric',
            'clicks' => 'integer',
            'cpc' => 'numeric',
            'impressions' => 'integer',
            'checkout_int' => 'integer',
        ];
    }
}
