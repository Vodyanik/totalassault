<?php

declare(strict_types = 1);

namespace App\Dev\Requests\Project;

use App\Dev\Requests\BaseRequest;

class Update extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return $this->can('update-project');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'id' => 'required|integer|exists:projects,id',
            'name' => 'required|max:191',
            'image' => 'file|image',
            'image_2' => 'file|image',
            'facebook_id' => 'max:20',
            'twitter_id' => 'max:20',
            'instagram_id' => 'max:20',
            'youtube_id' => 'max:20',
            'soundcloud_id' => 'max:20',
            'publicity_status' => 'boolean',
            'publicity_bc_team_id' => 'integer',
            'assets' => 'boolean',
            'status' => 'boolean',
        ];
    }
}
