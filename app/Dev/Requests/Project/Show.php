<?php

declare(strict_types = 1);

namespace App\Dev\Requests\Project;

use App\Dev\Requests\BaseRequest;

class Show extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return (bool) $this->can('read-project');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
