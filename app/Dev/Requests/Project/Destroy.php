<?php

declare(strict_types = 1);

namespace App\Dev\Requests\Project;

use App\Dev\Requests\BaseRequest;

class Destroy extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return $this->can('delete-project');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'id' => 'required|integer|exists:projects,id'
        ];
    }
}
