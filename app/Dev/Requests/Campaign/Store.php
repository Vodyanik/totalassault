<?php

declare(strict_types = 1);

namespace App\Dev\Requests\Campaign;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        // TODO: Update these rules to include required where it is needed
        return [
            'name' => 'required|max:191',
            'status' => 'integer',
            'report' => 'boolean',
            'user_id' => 'required|integer|exists:users,id',
            'purchases' => 'integer',
            'clicks' => 'integer',
            'impressions' => 'integer',
            'checkout_init' => 'integer',
            'budget' => 'numeric',
            'spend' => 'numeric',
            'remaining' => 'numeric',
            'cpa' => 'numeric',
            'conversion_value' => 'numeric',
            'nev' => 'numeric',
            'roas' => 'numeric',
            'cost_per_click' => 'numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ];
    }
}
