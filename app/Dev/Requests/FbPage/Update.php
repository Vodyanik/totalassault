<?php

declare(strict_types = 1);

namespace App\Dev\Requests\FbPage;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        // TODO: Update these rules to include required where it is needed

        return [
            'id' => 'required|integer|exists:fb_pages,id',
            'name' =>'required|max:255',
            'username' =>'required|max:255',
            'link' =>'active_url',
            'category_id' =>'integer',
            'access_token' =>'max:255',
            'auth_user_id' => 'integer',
            'auth_user_name' =>'max:255',
            'auth_user_token' =>'max:255',
            'status' => 'integer',
        ];
    }
}
