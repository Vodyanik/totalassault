<?php

declare(strict_types = 1);

namespace App\Dev\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'id' => 'required|integer|exists:companies,id',
            'name' => 'required|max:191',
            'image' => 'required|file|image',
            'email' => 'required|email',
            'phone' => 'required|string|max:30',
            'address_1' => 'required|max:191',
            'address_2' => 'max:191',
            'city_id' => 'numeric',
            'state_id' => 'numeric',
            'country_id' => 'numeric',
            'zip' => 'string',
            'timezone' => 'timezone',
            'currency' => 'max:5',
        ];
    }
}
