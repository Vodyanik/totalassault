<?php

/**
 * Create a shorter alias for the DIRECTORY_SEPARATOR constant
 */
define('DS', DIRECTORY_SEPARATOR);

if (!function_exists('push_to_request')) {
    /**
     * Add a value to the current request
     *
     * @param $key
     * @param $value
     */
    function push_to_request($key, $value)
    {
        request()->request->add([$key => $value]);
    }
}

if (!function_exists('storage_url')) {
    /**
     * Create a storage url
     *
     * @param string $url
     * @return string
     */
    function storage_url(string $url) : string
    {
        return url('/storage' . $url);
    }
}


if (!function_exists('create_dir')) {
    /**
     * Check to see if a path exists, create it if not
     *
     * @param string $path
     * @return bool|null
     */
    function create_dir(string $path) : ?bool
    {
        clearstatcache();
        if (!file_exists($path)) {
            return mkdir($path, 0755, true);
        }
        return null;
    }
}


if (!function_exists('path_is_image')) {
    /**
     * Check to see if a certain path represents an image file
     *
     * @param string $path
     * @param bool $storage_path
     * @return bool
     * @throws Exception
     */
    function path_is_image(string $path, bool $storage_path = true) : bool
    {
        clearstatcache();
        if ($storage_path) {
            $path = storage_path('/app/public' . $path);
        }

        if (!file_exists($path)) {
            throw new Exception("The given path is invalid, it doesn't point to an existing file");
        }

        try {
            $image_size = getimagesize($path);
        } catch (Exception $e) {
            return false;
        }

        return (bool) is_array($image_size);
    }
}

if (!function_exists('format_date_for_mysql')) {
    /**
     * Format a date to use with mysql
     *
     * @param string $date
     * @return string
     */
    function format_date_for_mysql(string $date) : string
    {
        $timestamp = strtotime($date);

        return date('Y-m-d H:i:s', $timestamp);
    }
}
