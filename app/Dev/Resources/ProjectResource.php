<?php

declare(strict_types = 1);

namespace App\Dev\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Dev\Models\Project;

class ProjectResource extends Resource
{
    /**
     * Convert a model object to an array
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image ? Project::IMG_DIR . DS . $this->image : '',
            'image_2' => $this->image_2 ? Project::IMG_DIR . DS . $this->image_2 : '',
            'facebook_id' => $this->facebook_id,
            'twitter_id' => $this->twitter_id,
            'instagram_id' => $this->instagram_id,
            'youtube_id' => $this->youtube_id,
            'soundcloud_id' => $this->soundcloud_id,
            'publicity_status' => $this->publicity_status,
            'publicity_bc_team_id' => $this->publicity_bc_team_id,
            'assets' => $this->assets,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
