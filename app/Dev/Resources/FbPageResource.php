<?php

declare(strict_types = 1);

namespace App\Dev\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FbPageResource extends Resource
{
    /**
     * Convert a model object to an array
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'link' => $this->link,
            'category_id' => $this->category_id,
            'access_token' => $this->access_token,
            'auth_user_id' => $this->auth_user_id,
            'auth_user_name' => $this->auth_user_name,
            'auth_user_token' => $this->auth_user_token,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
