<?php

declare(strict_types = 1);

namespace App\Dev\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Convert a model object to an array
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'company_id' => $this->company_id,
            'email' => $this->email,
            'office_phone' => $this->office_phone,
            'mobile_phone' => $this->mobile_phone,
            'job_title' => $this->job_title,
            'address_1' => $this->address_1,
            'address_2' => $this->address_2,
            'country_id' => $this->country_id,
            'city_id' => $this->city_id,
            'state_id' => $this->state_id,
            'zip' => $this->zip,
            'image' => $this->image,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
