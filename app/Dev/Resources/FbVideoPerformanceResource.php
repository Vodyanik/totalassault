<?php

namespace App\Dev\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FbVideoPerformanceResource extends Resource
{
    /**
     * Convert a model object to an array
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'campaign_id' => $this->campaign_id,
            'cost' => $this->cost,
            'impressions' => $this->impressions,
            'video_views_3_seconds' => $this->video_views_3_seconds,
            'video_views_10_seconds' => $this->video_views_10_seconds,
            'video_views_30_seconds' => $this->video_views_30_seconds,
            'video_views_25_percents' => $this->video_views_25_percents,
            'video_views_50_percents' => $this->video_views_50_percents,
            'video_views_75_percents' => $this->video_views_75_percents,
            'video_views_100_percents' => $this->video_views_100_percents,
            'avg_time_watched' => $this->avg_time_watched,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
