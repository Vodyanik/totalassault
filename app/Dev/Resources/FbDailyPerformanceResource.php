<?php

declare(strict_types = 1);

namespace App\Dev\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FbDailyPerformanceResource extends Resource
{
    /**
     * Convert a model object to an array
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'ad_id' => $this->ad_id,
            'adset_id' => $this->adset_id,
            'user_id' => $this->user_id,
            'campaign_id' => $this->campaign_id,
            'date_start' => $this->date_start,
            'date_stop' => $this->date_stop,
            'spend' => $this->spend,
            'purchases' => $this->purchases,
            'cpa' => $this->cpa,
            'conv_value' => $this->conv_value,
            'net' => $this->net,
            'roas' => $this->roas,
            'clicks' => $this->clicks,
            'cpc' => $this->cpc,
            'impressions' => $this->impressions,
            'checkout_int' => $this->checkout_int,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
