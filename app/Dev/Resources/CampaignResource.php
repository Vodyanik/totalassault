<?php

declare(strict_types = 1);

namespace App\Dev\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CampaignResource extends Resource
{
    /**
     * Convert a model object to an array
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
            'report' => $this->status,
            'user_id' => $this->user_id,
            'purchases' => $this->purchases,
            'clicks' => $this->clicks,
            'impressions' => $this->impressions,
            'checkout_init' => $this->checkout_init,
            'budget' => $this->budget,
            'spend' => $this->spend,
            'remaining' => $this->remaining,
            'cpa' => $this->cpa,
            'conversion_value' => $this->conversion_value,
            'nev' => $this->nev,
            'roas' => $this->roas,
            'cost_per_click' => $this->cost_per_click,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
