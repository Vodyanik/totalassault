<?php

namespace App\Dev\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FbCanvasPerformanceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'campaign_id' => $this->campaign_id,
            'cost' => $this->cost,
            'impressions' => $this->impressions,
            'cpm' => $this->cpm,
            'actions' => $this->actions,
            'cpa' => $this->cpa,
            'canvas_opens' => $this->canvas_opens,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
