<?php

declare(strict_types = 1);

namespace App\Dev\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Dev\Models\Company;

class CompanyResource extends Resource
{
    /**
     * Convert a model object to an array
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image ? Company::IMG_DIR . DS . $this->image : '',
            'email' => $this->email,
            'phone' => $this->phone,
            'address_1' => $this->address_1,
            'address_2' => $this->address_2,
            'city_id' => $this->city_id,
            'state_id' => $this->state_id,
            'country_id' => $this->country_id,
            'zip' => $this->postcode,
            'timezone' => $this->timezone,
            'currency' => $this->currencyCode,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
