<?php

use Illuminate\Support\Facades\Route;

$withoutHtmlRoutes = ['except' => ['create', 'edit']];

Route::resource('/projects', 'ProjectController', $withoutHtmlRoutes);
Route::resource('/users', 'UserController', $withoutHtmlRoutes);
Route::resource('/companies', 'CompanyController', $withoutHtmlRoutes);
Route::prefix('/advertising')->group(function () use ($withoutHtmlRoutes) {
    Route::resource('/campaigns', 'CampaignController', $withoutHtmlRoutes);
    Route::prefix('/facebook')->group(function () use ($withoutHtmlRoutes) {
        Route::get('/daily-performance/export/csv', 'Facebook\DailyPerformanceController@exportCsv');
        Route::resource('/daily-performance', 'Facebook\DailyPerformanceController', $withoutHtmlRoutes);
        Route::resource('/video-performance', 'Facebook\VideoPerformanceController', $withoutHtmlRoutes);
        Route::resource('/pages', 'Facebook\PageController', $withoutHtmlRoutes);
        Route::resource('/canvas-performance', 'Facebook\CanvasPerformanceController', $withoutHtmlRoutes);
    });
});
