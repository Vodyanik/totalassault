<?php

declare(strict_types = 1);

namespace App\Dev\Repositories;

use App\Dev\Models\User;
use App\Abstractions\Repository;
use App\Dev\Contracts\Repositories\UserRepository as RepositoryContract;

class UserRepository extends Repository implements RepositoryContract
{
    /**
     * Return the model to power up this repository
     *
     * @return string
     */
    public function entity() : string
    {
        return User::class;
    }
}
