<?php
declare(strict_types = 1);

namespace App\Dev\Repositories;

use App\Dev\Models\Facebook\Page;
use App\Abstractions\Repository;
use App\Dev\Contracts\Repositories\FbPageRepository as RepositoryContract;

class FbPageRepository extends Repository implements RepositoryContract
{
    /**
     * Return the model to power up this repository
     *
     * @return string
     */
    public function entity() : string
    {
        return Page::class;
    }
}
