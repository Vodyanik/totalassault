<?php

declare(strict_types = 1);

namespace App\Dev\Repositories;

use App\Dev\Models\Project;
use App\Abstractions\Repository;
use App\Dev\Contracts\Repositories\ProjectRepository as RepositoryContract;

class ProjectRepository extends Repository implements RepositoryContract
{
    /**
     * Return the model to power up this repository
     *
     * @return string
     */
    public function entity() : string
    {
        return Project::class;
    }
}
