<?php

namespace App\Dev\Repositories;

use App\Abstractions\Repository;
use App\Dev\Contracts\Repositories\FbVideoPerformanceRepository as RepositoryContract;
use App\Dev\Models\Facebook\VideoPerformance;

class FbVideoPerformanceRepository extends Repository implements RepositoryContract
{
    /**
     * Return the model to power up this repository
     *
     * @return string
     */
    public function entity() : string
    {
        return VideoPerformance::class;
    }

}
