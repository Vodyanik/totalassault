<?php

namespace App\Dev\Repositories;

use App\Abstractions\Repository;
use App\Dev\Contracts\Repositories\FbCanvasPerformanceRepository as RepositoryContract;
use App\Dev\Models\Facebook\CanvasPerformance;

class FbCanvasPerformanceRepository extends Repository implements RepositoryContract
{
    /**
     * Return the model to power up this repository
     *
     * @return string
     */
    public function entity() : string
    {
        return CanvasPerformance::class;
    }

}
