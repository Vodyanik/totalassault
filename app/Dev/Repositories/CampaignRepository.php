<?php

declare(strict_types = 1);

namespace App\Dev\Repositories;

use App\Dev\Models\Campaign;
use App\Abstractions\Repository;
use App\Dev\Contracts\Repositories\CampaignRepository as RepositoryContract;

class CampaignRepository extends Repository implements RepositoryContract
{
    /**
     * Return the model to power up this repository
     *
     * @return string
     */
    public function entity() : string
    {
        return Campaign::class;
    }
}
