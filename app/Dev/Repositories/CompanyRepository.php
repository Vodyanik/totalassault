<?php

declare(strict_types = 1);

namespace App\Dev\Repositories;

use App\Dev\Models\Company;
use App\Abstractions\Repository;
use App\Dev\Contracts\Repositories\CompanyRepository as RepositoryContract;

class CompanyRepository extends Repository implements RepositoryContract
{
    /**
     * Return the model to power up this repository
     *
     * @return string
     */
    public function entity() : string
    {
        return Company::class;
    }
}
