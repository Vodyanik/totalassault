<?php

declare(strict_types = 1);

namespace App\Dev\Repositories;

use App\Abstractions\Repository;
use App\Dev\Contracts\Repositories\FbDailyPerformanceRepository as RepositoryContract;
use App\Dev\Models\Facebook\DailyPerformance;

class FbDailyPerformanceRepository extends Repository implements RepositoryContract
{
    /**
     * Return the model to power up this repository
     *
     * @return string
     */
    public function entity() : string
    {
        return DailyPerformance::class;
    }
}
