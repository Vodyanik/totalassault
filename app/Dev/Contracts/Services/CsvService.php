<?php

namespace App\Dev\Contracts\Services;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Abstractions\Contracts\Repository;

interface CsvService
{
    public function export(Request $request, Repository $repository) : Response;
}
