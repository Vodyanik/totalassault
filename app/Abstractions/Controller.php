<?php

namespace App\Abstractions;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Optimus\Bruno\LaravelController as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var Service
     */
    protected $service;

    /**
     * Parse the GET parameters for API filtering
     * Do extra checks and return the parsed options
     *
     * @return array
     */
    public function getResourceOptions()
    {
        $options = $this->parseResourceOptions();

        if (is_null($options['limit'])) {
            $options['limit'] = 20;
        }

        if (is_null($options['page'])) {
            $options['page'] = 0;
        }

        return $options;
    }
}
