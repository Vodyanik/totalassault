<?php

namespace App\Abstractions\Contracts;

interface Criteria
{
    public function withCriteria(...$criteria);
}
