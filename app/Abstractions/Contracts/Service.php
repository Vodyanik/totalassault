<?php

namespace App\Abstractions\Contracts;

use Illuminate\Http\Request;

interface Service
{
    public function get();
    public function show(string $id);
    public function store(Request $request);
    public function update(Request $request, string $id);
    public function destroy(string $id);
}
