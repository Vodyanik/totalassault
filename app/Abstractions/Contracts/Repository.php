<?php

namespace App\Abstractions\Contracts;

interface Repository
{
    public function get();
    public function all();
    public function find(string $id);
    public function findWhere($column, $value);
    public function findWhereFirst($column, $value);
    public function findOrCreate($attributes, $values = null);
    public function paginate($perPage = 20);
    public function paginateOptions(array $options);
    public function whereIf($column, $value);
    public function limit($limit = 20);
    public function orderBy($column, $direction = 'ASC');
    public function create(array $data);
    public function createRelated($entity, $relation, array $data);
    public function attachRelated($entity, $relation, array $data);
    public function detachRelated($entity, $relation, array $data);
    public function update(string $id, array $data);
    public function sync($entity, $relation, array $data);
    public function destroy(string $id);
    public function withCriteria(...$criteria);
    public function withCount($relations);
    public function whereBetween(string $field, array $range);
    public function select(array $columns);
}
