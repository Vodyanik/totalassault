<?php

namespace App\Abstractions;

use Illuminate\Http\Request;
use App\Abstractions\Controller;
use App\Abstractions\Contracts\Repository;

use App\Abstractions\Contracts\Service as ServiceContract;

abstract class Service implements ServiceContract
{
    protected $repository;

    /**
     * Service constructor.
     *
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Return a collection of records
     *
     * @return mixed
     */
    public function get()
    {
        $controller = new Controller();
        $options = $controller->getResourceOptions();
        return $this->repository->paginateOptions($options);
    }

    /**
     * Return a single record
     *
     * @param int $id
     * @return mixed
     */
    public function show(string $id)
    {
        return $this->repository->find($id);
    }

    /**
     * Store a record
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update an existing record
     *
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, string $id)
    {
        return $this->repository->update($id, $request->all());
    }

    /**
     * Destroy an existing record
     *
     * @param int $id
     * @return mixed
     */
    public function destroy(string $id)
    {
        return $this->repository->destroy($id);
    }
}
