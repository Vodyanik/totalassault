<?php

namespace App\Abstractions;

use App\Abstractions\Exceptions\NoEntityDefined;
use App\Abstractions\Contracts\Criteria;
use App\Abstractions\Contracts\Repository as RepositoryContract;
use Optimus\Bruno\EloquentBuilderTrait;

abstract class Repository implements RepositoryContract, Criteria
{
    // TODO: Write proper comments for every single method
    // TODO: Add argument and return typehints
    // TODO: Unit test the class

    use EloquentBuilderTrait;

    /**
     * The current Eloquent Model used for the Repository
     *
     * @var mixed
     */
    protected $entity;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        try {
            $this->entity = $this->resolveEntity();
        } catch (\Exception $e) {
            die("Couldn't instantiate the Repository class");
        }
    }

    /**
     * @return mixed
     * @throws NoEntityDefined
     */
    protected function resolveEntity()
    {
        if (!method_exists($this, 'entity')) {
            throw new NoEntityDefined();
        }

        return app()->make($this->entity());
    }

    /**
     * @throws NoEntityDefined
     */
    public function resolve()
    {
        $this->entity = $this->resolveEntity();
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->get();
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->entity->get();
    }

    /**
     * @return array
     */
    public function ids() : array
    {
        return $this->entity->pluck('id')->toArray();
    }

    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function findWhere($column, $value)
    {
        return $this->entity->where($column, $value);
    }

    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function findWhereFirst($column, $value)
    {
        return $this->entity->where($column, $value)->firstOrFail();
    }

    /**
     * @param $attributes
     * @param null $values
     * @return mixed
     */
    public function findOrCreate($attributes, $values = null)
    {
        return $this->entity->firstOrCreate($attributes, $values ?? $attributes);
    }

    /**
     * @param int $perPage
     * @return mixed
     */
    public function paginate($perPage = 20)
    {
        return $this->entity->paginate($perPage);
    }

    /**
     * @param $column
     * @param $value
     * @return $this
     */
    public function whereIf($column, $value)
    {
        if (false !== $value) {
            $this->entity->where($column, $value);
        }

        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function limit($limit = 20)
    {
        $this->entity->limit($limit);
        return $this;
    }

    /**
     * @param $column
     * @param string $direction
     * @return $this
     */
    public function orderBy($column, $direction = 'ASC')
    {
        $this->entity->orderBy($column, $direction);
        return $this;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->entity->create($data);
    }

    /**
     * @param array $data
     * @return \Illuminate\Support\Collection
     */
    public function createMany(array $data)
    {
        $collection = collect();
        foreach ($data as $item) {
            $collection->push($this->entity->create($item));
        }

        return  $collection;
    }

    /**
     * @param $entity
     * @param $relation
     * @param array $data
     * @return mixed
     */
    public function saveMany($entity, $relation, array $data)
    {
        return $entity->{$relation}()->saveMany($data);
    }

    /**
     * @param $entity
     * @param $relation
     * @param array $data
     * @return mixed
     */
    public function createRelated($entity, $relation, array $data)
    {
        $this->loadEntity($entity);

        return $entity->{$relation}()->createMany($data);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function find(string $id)
    {
        return $this->entity->findOrFail($id);
    }

    /**
     * @param $entity
     * @param $relation
     * @param array $data
     * @return mixed
     */
    public function attachRelated($entity, $relation, array $data)
    {
        $this->loadEntity($entity);

        return $entity->{$relation}()->attach($data);
    }

    /**
     * @param $entity
     */
    protected function loadEntity(&$entity)
    {
        if (!is_object($entity)) {
            $entity = $this->find($entity);
        }
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(string $id, array $data)
    {
        $entity = $this->find($id);
        $entity->update($data);

        return $entity;
    }

    /**
     * @param $entity
     * @param $relation
     * @param array $data
     * @return mixed
     */
    public function sync($entity, $relation, array $data)
    {
        $this->loadEntity($entity);
        return $entity->{$relation}()->sync($data);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function destroy(string $id)
    {
        $entity = $this->find($id);
        $entity->delete();

        return $entity;
    }

    /**
     * @param $entity
     * @param $relation
     * @return mixed
     */
    public function destroyAssociations($entity, $relation)
    {
        return $this->detachRelated($entity, $relation, []);
    }

    /**
     * @param $entity
     * @param $relation
     * @param array $data
     * @return mixed
     */
    public function detachRelated($entity, $relation, array $data)
    {
        $this->loadEntity($entity);
        return $entity->{$relation}()->detach(empty($data) ? null : $data);
    }

    /**
     * @param array ...$criteria
     * @return $this
     */
    public function withCriteria(...$criteria)
    {
        $criteria = array_flatten($criteria);

        foreach ($criteria as $criterion) {
            $this->entity = $criterion->apply($this->entity);
        }

        return $this;
    }

    /**
     * @param $relations
     * @return $this
     */
    public function withCount($relations)
    {
        $this->entity->withCount($relations);
        return $this;
    }

    /**
     * @param array $options
     * @return mixed
     */
    public function paginateOptions(array $options)
    {
        if ($options['limit'] == "null") {
            unset($options['limit']);
            unset($options['page']);
        }

        $query = $this->entity::query();
        $this->applyResourceOptions($query, $options);

        if (empty($options['limit'])) {
            $total_rows = $query->count();
            return $query->paginate($total_rows);
        }

        return $query->paginate($options['limit']);
    }

    /**
     * Filter the records using a lower and upper limit
     *
     * @param string $field
     * @param array $rangeArray
     * @return mixed
     */
    public function whereBetween(string $field, array $range)
    {
        return $this->entity->whereBetween($field, $range);
    }

    /**
     * Choose what columns to retrieve
     *
     * @param array $columns
     * @return mixed
     */
    public function select(array $columns)
    {
        return $this->entity->select($columns);
    }
}
