<?php

use Faker\Generator as Faker;
use App\Dev\Models\Facebook\VideoPerformance;
use App\Dev\Models\Campaign;

$factory->define(VideoPerformance::class, function (Faker $faker) {
    $campaign = Campaign::inRandomOrder()->first();

    return [
        'campaign_id' => $campaign->id,
        'cost' => rand(100, 1000),
        'impressions' => rand(0, 1000),
        'video_views_3_seconds' => rand(0, 1000000),
        'video_views_10_seconds' => rand(0, 1000000),
        'video_views_30_seconds' => rand(0, 1000000),
        'video_views_25_percents' => rand(0, 1000000),
        'video_views_50_percents' => rand(0, 1000000),
        'video_views_75_percents' => rand(0, 1000000),
        'video_views_100_percents' => rand(0, 1000000),
        'avg_time_watched' => rand(0, 1000),
    ];
});
