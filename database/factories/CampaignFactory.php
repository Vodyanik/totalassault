<?php

use Faker\Generator as Faker;
use \App\Dev\Models\User;
use \App\Dev\Models\Campaign;
use Carbon\Carbon;

$factory->define(Campaign::class, function (Faker $faker) {
    $user = User::inRandomOrder()->first();
    $start_days = rand(0, 50);
    $end_days = rand(0, 50);
    $start_date = Carbon::now()->addDays($start_days);
    $end_date = $start_date->addDays($end_days);

    return [
        'name' => ucfirst($faker->word()),
        'report' => rand(0, 1),
        'status' => rand(0, 4),
        'user_id' => $user->id,
        'purchases' => rand(0, 1000),
        'clicks' => rand(0, 1000),
        'impressions' => rand(0, 10000),
        'checkout_init' => 0,
        'budget' => rand(0, 1000),
        'spend' => rand(0, 1000),
        'remaining' => rand(0, 1000),
        'cpa' => rand(0, 1000),
        'conversion_value' => rand(0, 1000),
        'nev' => rand(0, 1000),
        'roas' => rand(0, 1000),
        'cost_per_click' => rand(0, 50),
        'start_date' => $start_date->format('Y-m-d'),
        'end_date' => $end_date->format('Y-m-d')
    ];
});
