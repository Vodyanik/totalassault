<?php

use Faker\Generator as Faker;
use App\Dev\Models\Facebook\CanvasPerformance;
use App\Dev\Models\Campaign;

$factory->define(CanvasPerformance::class, function (Faker $faker) {
    $campaign = Campaign::inRandomOrder()->first();

    return [
        'campaign_id' => $campaign->id,
        'cost' => rand(10, 1000),
        'impressions' => rand(1, 1000),
        'cpm' => rand(1, 1000),
        'actions' => rand(1, 1000),
        'cpa' => rand(1, 1000),
        'canvas_opens' => rand(1, 1000),
    ];
});
