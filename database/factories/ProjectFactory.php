<?php

use Faker\Generator as Faker;
use \App\Dev\Models\Project;

$factory->define(Project::class, function (Faker $faker) {
    $image_dir = storage_path("app" . DS . "public" . Project::IMG_DIR);
    create_dir($image_dir);
    $image = $faker->image($image_dir, 1000, 600, 'cats', false);

    return [
        'name' => $faker->sentence(7),
        'image' => $image,
        'image_2' => $image,
        'facebook_id' => $faker->randomNumber(8),
        'twitter_id' => $faker->randomNumber(8),
        'instagram_id' => $faker->randomNumber(8),
        'youtube_id' => $faker->randomNumber(8),
        'soundcloud_id' => $faker->randomNumber(8),
        'publicity_status' => rand(0, 1),
        'publicity_bc_team_id' => rand(0, 100),
        'assets' => rand(0, 1),
        'status' => rand(0, 1),
    ];
});
