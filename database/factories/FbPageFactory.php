<?php

use App\Dev\Models\Facebook\Page;
use Faker\Generator as Faker;

$factory->define(Page::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->userName,
        'link' => $faker->url,
        'category_id' => rand(1, 1000),
        'access_token' => str_random(10),
        'auth_user_id' => rand(1, 1000),
        'auth_user_name' => $faker->userName,
        'auth_user_token' => str_random(10),
        'status' => $faker->randomDigit,
    ];
});

