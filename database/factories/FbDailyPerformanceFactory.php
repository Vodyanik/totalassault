<?php

use Faker\Generator as Faker;
use App\Dev\Models\Facebook\DailyPerformance;
use Carbon\Carbon;

$factory->define(DailyPerformance::class, function (Faker $faker) {
    $start_days = rand(0, 50);
    $end_days = rand(0, 50);
    $start_date = Carbon::now()->addDays($start_days);
    $end_date = $start_date->addDays($end_days);

    return [
        'ad_id' => rand(0, 1000),
        'adset_id' => rand(0, 1000),
        'user_id' => rand(0, 100),
        'campaign_id' => rand(0, 50),
        'date_start' => $start_date->format('Y-m-d'),
        'date_stop' => $end_date->format('Y-m-d'),
        'spend' => rand(0, 1000),
        'purchases' => rand(0, 1000),
        'cpa' => rand(0, 1000),
        'conv_value' => rand(0, 1000),
        'net' => rand(0, 1000),
        'roas' => rand(0, 1000),
        'clicks' => rand(0, 1000),
        'cpc' => rand(0, 1000),
        'impressions' => rand(0, 10000),
        'checkout_int' => 0,
    ];
});
