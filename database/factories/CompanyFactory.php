<?php

use Faker\Generator as Faker;

use App\Dev\Models\Company;

$factory->define(Company::class, function (Faker $faker) {
    $image_dir = storage_path("app" . DS . "public" . Company::IMG_DIR);
    create_dir($image_dir);
    $image = $faker->image($image_dir, 1000, 600, 'cats', false);

    return [
        'name' => $faker->company,
        'email' => $faker->unique()->safeEmail,
        'image' => $image,
        'phone' => $faker->phoneNumber,
        'address_1' => $faker->streetAddress,
        'address_2' => $faker->streetAddress,
        'city_id' => rand(1, 50),
        'state_id' => rand(1, 50),
        'country_id' => rand(1, 50),
        'zip' => $faker->postcode,
        'timezone' => $faker->timezone,
        'currency' => $faker->currencyCode
    ];
});
