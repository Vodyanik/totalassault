<?php

use Faker\Generator as Faker;
use App\Dev\Models\User;

$factory->define(User::class, function (Faker $faker) {
    $email = $faker->unique()->safeEmail;
    $password = bcrypt('secret');
    $image_dir = storage_path("app" . DS . "public" . User::IMG_DIR);
    create_dir($image_dir);
    $image = $faker->image($image_dir, 1000, 600, 'cats', false);

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'company_id' => rand(1, 10),
        'email' => $email,
        'password' => $password,
        'office_phone' => $faker->phoneNumber,
        'mobile_phone' => $faker->phoneNumber,
        'job_title' => $faker->jobTitle,
        'address_1' => $faker->streetAddress,
        'address_2' => $faker->streetAddress,
        'country_id' => rand(1, 50),
        'city_id' => rand(1, 50),
        'state_id' => rand(1, 50),
        'zip' => $faker->postcode,
        'image' => $image,
    ];
});
