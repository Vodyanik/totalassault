<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFbVideoPerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_video_performance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->float('cost');
            $table->integer('impressions');
            $table->integer('video_views_3_seconds');
            $table->integer('video_views_10_seconds');
            $table->integer('video_views_30_seconds');
            $table->integer('video_views_25_percents');
            $table->integer('video_views_50_percents');
            $table->integer('video_views_75_percents');
            $table->integer('video_views_100_percents');
            $table->integer('avg_time_watched');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_video_performance');
    }
}
