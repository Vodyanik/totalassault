<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('report');
            $table->integer('status');
            $table->integer('user_id');
            $table->integer('purchases');
            $table->integer('clicks');
            $table->integer('impressions');
            $table->integer('checkout_init');
            $table->float('budget');
            $table->float('spend');
            $table->float('remaining');
            $table->float('cpa');
            $table->float('conversion_value');
            $table->float('nev');
            $table->float('roas');
            $table->float('cost_per_click');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
