<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFbDailyPerformanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_daily_performance', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('ad_id');
            $table->bigInteger('adset_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('campaign_id')->unsigned();
            $table->date('date_start');
            $table->date('date_stop');
            $table->float('spend');
            $table->integer('purchases');
            $table->float('cpa');
            $table->float('conv_value');
            $table->float('net');
            $table->float('roas');
            $table->integer('clicks');
            $table->float('cpc');
            $table->integer('impressions');
            $table->integer('checkout_int');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_daily_performance');
    }
}
