<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookCanvasPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_canvas_performance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->float('cost');
            $table->integer('impressions');
            $table->float('cpm', 8, 3);
            $table->integer('actions');
            $table->float('cpa', 8, 3);
            $table->integer('canvas_opens');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_canvas_performance');
    }
}
