<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_pages', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name',255);
            $table->string('username',255);
            $table->string('link');
            $table->integer('category_id');
            $table->string('access_token',255);
            $table->bigInteger('auth_user_id')->unsigned();
            $table->string('auth_user_name',255);
            $table->string('auth_user_token',255);
            $table->tinyInteger('status')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_pages');
    }
}
