<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompaniesTableSeeder::class);
        $this->call(LaratrustSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(CampaignsTableSeeder::class);
        $this->call(FbDailyPerformanceTableSeeder::class);
        $this->call(FbVideoPerformanceTableSeeder::class);
        $this->call(FbPagesTableSeeder::class);
        $this->call(FbCanvasPerformanceTableSeeder::class);
    }
}
