<?php

use Illuminate\Database\Seeder;
use App\Dev\Models\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Project::class, 30)->create();
    }
}
