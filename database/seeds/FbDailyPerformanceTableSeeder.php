<?php

use Illuminate\Database\Seeder;
use App\Dev\Models\Facebook\DailyPerformance;

class FbDailyPerformanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DailyPerformance::class, 200)->create();
    }
}
