<?php

use Illuminate\Database\Seeder;
use App\Dev\Models\Facebook\VideoPerformance;

class FbVideoPerformanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(VideoPerformance::class, 30)->create();
    }
}
