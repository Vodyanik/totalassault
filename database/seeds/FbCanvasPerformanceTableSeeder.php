<?php

use Illuminate\Database\Seeder;
use App\Dev\Models\Facebook\CanvasPerformance;

class FbCanvasPerformanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CanvasPerformance::class, 30)->create();
    }
}
