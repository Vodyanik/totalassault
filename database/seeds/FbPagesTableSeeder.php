<?php

use Illuminate\Database\Seeder;
use App\Dev\Models\Facebook\Page;

class FbPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Page::class, 30)->create();
    }
}
