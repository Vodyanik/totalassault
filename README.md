# TARA

Total Assault Reporting & Analytics

This project aims to improve, update, and replace Total Assault’s current, outdated, proprietary backend reporting and analytics web app.

### Technical description

The project is a SPA (Single Page Application) powered by Vue.js on the frontend while the backend is a Laravel API that serves JSON.

### Credentials

| Email | Password |
| ------ | ----------- |
| admin@tara.com   | secret |
| client@tara.com   | secret |
| developer@tara.com   | secret |

### Development Instructions

Given we are a team of multiple developers working on the project, we need to install some plugins in order to make the code consistent

#### Development Plugins

__These plugins are a must have in order to work on the project!__ 

* EditorConfig (http://editorconfig.org/) - Ensures indentation consistency
* PHP_CodeSniffer (https://github.com/squizlabs/PHP_CodeSniffer) - PSR-2 enforcement
* JSHint (http://jshint.com/about/) - Javascript standard enforcement (We will migrate to ESlint when I get the chance to create a proper config file for it)

_Note_: Use the PSR-2 setting for PHP_CodeSniffer and make sure JSHint uses the .jshintrc config file.

#### Git Workflow

First thing first: __NEVER EVER EVER COMMIT TO MASTER DIRECTLY__.

Create a new branch for every single feature, the branches must have descriptive names like: `crud-for-users` or `fix-dashboard-datatables`, __don't use the `feature/` prefix__. 

When the branch is ready, create a pull request to `master` and add me (Florian) as a reviewer.

Ideally the pull request should contain functional tests (they are optional though). 

I will review the code and give you advice on what needs to be improved on that branch.
You will push commits to that branch untill all issues are solved, at that point I will merge the branch to master.

__You should never merge your own branch in master unless I specifically told you to do so__.

The flow that I'm describing above is more or less the Github Flow, if you are not familiar with it, click the link bellow to see a tutorial

* GitHub Flow (https://guides.github.com/introduction/flow/)

#### Recommended Software

* PhpStorm or Visual Studio Code
* Homestead or Docker

### Installation

* Run in terminal `npm install`
* Run in terminal `composer update`
* Copy the `.env.example` file to `.env`
* Update the `.env` file with the database configurations
* Run in terminal `find . -type d -exec chmod 755 {} \;`
* Run in terminal `find . -type f -exec chmod 644 {} \;`
* Run in terminal `chmod -R 777 storage bootstrap/cache`
* Run in terminal `php artisan key:generate`
* Run in terminal `php artisan storage:link`
* Run in terminal `php artisan migrate:refresh --seed`
* Run in terminal `php artisan passport:install --force`
* Copy client key with the ID of 2
* Update the `.env` file (located in the root of the project) with the client key you just copied
* Copy the `/frontend/js/.env.example.js` file to `.env.js`
* Update the `.env.js` file with the proper app urls
* All frontend assets are compiled with `npm run production`
* Only the development assets are compiled with `npm run dev` (without libraries)

### The Metronic theme

We are using the latest version of the Metronic theme.

The HTML code of the theme is found inside: 

`metronic_v5.0.7/metronic_v5.0.7/default/dist/default`

### Project structure

These are the directories that you will be working in 90% of the time: 

```
root
└───app
│   └───Dev
└───frontend
│   └───js
│   └───scss
│   └───tests
└───tests
    └───Integration
    └───Functional
```

The backend devs will do all of the work inside `app/Dev` and `tests` directories, while the frontend devs will work exclusively inside of the `frontend` dir.
