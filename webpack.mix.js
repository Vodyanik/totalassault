const mix = require('laravel-mix');
const path = require('path');

mix.webpackConfig({
  resolve: {
    modules: [
      path.resolve('./frontend/js'),
      path.resolve('./node_modules'),
    ],
  },
});

mix
  .js('frontend/js/app.js', 'public/js')
  .sass('frontend/sass/app.scss', 'public/css')
  .options({
    processCssUrls: false,
  });

if (mix.inProduction()) {
  mix
    .js('frontend/js/libs.js', 'public/js')
    .js('frontend/js/theme.js', 'public/js')
    .sass('frontend/sass/libs.scss', 'public/css')
    .sass('frontend/sass/theme.scss', 'public/css')
    .copy('node_modules/font-awesome/fonts/*', 'public/fonts')
    .copy('node_modules/line-awesome/dist/fonts/*', 'public/fonts')
    .copy('resources/fonts/*/*', 'public/fonts')
    .copy('resources/vendors/flaticon/fonts/*', 'public/fonts')
    .copy('resources/vendors/metronic/fonts/*', 'public/fonts')
    .version()
    .options({
      processCssUrls: false,
    });
}
