<?php

namespace Tests\Functional\Company;

use App\Dev\Models\Company;
use Illuminate\Http\UploadedFile;

class StoreTest extends Base
{
    /** @test */
    public function store_record()
    {
        // create contact array
        $record = factory(Company::class)->make()->toArray();
        $record['image'] = UploadedFile::fake()->image('image.jpg');

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('POST', $this->url, $record)
            ->assertStatus(201)
            ->assertJsonStructure($this->standard_json);

        // remove the fake images from the insert data
        unset($record['image']);

        // check to see if the record was stored
        $this->assertDatabaseHas($this->table, $record);

        $item = Company::where($record)->first();

        // check to see if the image file was created
        $this->imageExists($item->image);
    }
}
