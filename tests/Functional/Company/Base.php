<?php

namespace Tests\Functional\Company;

use Tests\Functional\FunctionalTestCase;
use App\Dev\Models\Company;
use Illuminate\Support\Facades\Storage;

class Base extends FunctionalTestCase
{
    /**
     * The expected structure of the JSON response
     */
    const JSON_STRUCTURE = [
        'id',
        'name',
        'image',
        'email',
        'phone',
        'address_1',
        'address_2',
        'city_id',
        'state_id',
        'country_id',
        'zip',
        'timezone',
        'currency',
        'updated_at',
        'created_at',
    ];

    /**
     * @var string  The base url for testing
     */
    protected $route = '/companies';

    /**
     * @var string The base table for the current resource
     */
    protected $table = 'companies';

    /**
     * Base constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->url = self::API_URL . $this->route;
        $this->updateJson(self::JSON_STRUCTURE);
    }

    public function imageExists(string $path)
    {
        $image_location = Company::IMG_DIR . DS . $path;
        Storage::assertExists($image_location);
        // delete the image
        Storage::delete($image_location);
    }

    public function imageMissing(string $path)
    {
        $image_location = Company::IMG_DIR . DS . $path;
        Storage::assertMissing($image_location);
    }
}
