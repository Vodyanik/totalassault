<?php

namespace Tests\Functional\Campaign;

use Tests\Functional\FunctionalTestCase;

class Base extends FunctionalTestCase
{
    /**
     * The expected structure of the JSON response
     */
    const JSON_STRUCTURE = [
        'name',
        'status',
        'report',
        'user_id',
        'purchases',
        'clicks',
        'impressions',
        'checkout_init',
        'budget',
        'cpa',
        'conversion_value',
        'nev',
        'roas',
        'cost_per_click',
        'start_date',
        'end_date',
        'updated_at',
        'created_at',
    ];

    /**
     * @var string  The base url for testing
     */
    protected $route = '/advertising/campaigns';

    /**
     * @var string The base table for the current resource
     */
    protected $table = 'campaigns';

    /**
     * Base constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->url = self::API_URL . $this->route;
        $this->updateJson(self::JSON_STRUCTURE);
    }
}
