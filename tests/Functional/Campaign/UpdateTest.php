<?php

namespace Tests\Functional\Campaign;

use App\Dev\Models\Campaign;

class UpdateTest extends Base
{
    /** @test */
    public function update_record()
    {
        // create a new record in the database
        $record_obj = factory(Campaign::class)->create();

        // generate the proper url
        $url = $this->url . '/' . $record_obj->id;

        // make update data
        $data = factory(Campaign::class)->make()->toArray();
        $data['id'] = $record_obj->id;

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('PUT', $url, $data)
            ->assertStatus(200)
            ->assertJsonStructure($this->standard_json);

        // check to see if the record was updated
        $this->assertDatabaseHas($this->table, $data);
    }
}
