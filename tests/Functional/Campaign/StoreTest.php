<?php

namespace Tests\Functional\Campaign;

use App\Dev\Models\Campaign;

class StoreTest extends Base
{
    /** @test */
    public function store_record()
    {
        // create contact array
        $record = factory(Campaign::class)->make()->toArray();

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('POST', $this->url, $record)
            ->assertStatus(201)
            ->assertJsonStructure($this->standard_json);

        // check to see if the record was stored
        $this->assertDatabaseHas($this->table, $record);
    }
}
