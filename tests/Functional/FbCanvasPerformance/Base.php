<?php

namespace Tests\Functional\FbCanvasPerformance;

use Tests\Functional\FunctionalTestCase;

class Base extends FunctionalTestCase
{
    /**
     * The expected structure of the JSON response
     */
    const JSON_STRUCTURE = [
        'campaign_id',
        'cost',
        'impressions',
        'cpm',
        'actions',
        'cpa',
        'canvas_opens',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string  The base url for testing
     */
    protected $route = '/advertising/facebook/canvas-performance';

    /**
     * @var string The base table for the current resource
     */
    protected $table = 'fb_canvas_performance';

    /**
     * Base constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->url = self::API_URL . $this->route;
        $this->updateJson(self::JSON_STRUCTURE);
    }
}
