<?php

namespace Tests\Functional\FbVideoPerformance;

use Tests\Functional\FunctionalTestCase;

class Base extends FunctionalTestCase
{
    /**
     * The expected structure of the JSON response
     */
    const JSON_STRUCTURE = [
        'campaign_id',
        'cost',
        'impressions',
        'video_views_3_seconds',
        'video_views_10_seconds',
        'video_views_30_seconds',
        'video_views_25_percents',
        'video_views_50_percents',
        'video_views_75_percents',
        'video_views_100_percents',
        'avg_time_watched',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string  The base url for testing
     */
    protected $route = '/advertising/facebook/video-performance';

    /**
     * @var string The base table for the current resource
     */
    protected $table = 'fb_video_performance';

    /**
     * Base constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->url = self::API_URL . $this->route;
        $this->updateJson(self::JSON_STRUCTURE);
    }
}
