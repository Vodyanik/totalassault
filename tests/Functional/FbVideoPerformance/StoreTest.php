<?php

namespace Tests\Functional\FbVideoPerformance;

use App\Dev\Models\Facebook\VideoPerformance;

class StoreTest extends Base
{
    /** @test */
    public function store_record()
    {
        // create contact array
        $record = factory(VideoPerformance::class)->make()->toArray();

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('POST', $this->url, $record)
            ->assertStatus(201)
            ->assertJsonStructure($this->standard_json);

        // check to see if the record was stored
        $this->assertDatabaseHas($this->table, $record);
    }
}
