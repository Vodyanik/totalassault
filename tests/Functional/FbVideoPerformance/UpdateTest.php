<?php

namespace Tests\Functional\FbVideoPerformance;

use App\Dev\Models\Facebook\VideoPerformance;

class UpdateTest extends Base
{
    /** @test */
    public function update_record()
    {
        // create a new record in the database
        $record_obj = factory(VideoPerformance::class)->create();

        // generate the proper url
        $url = $this->url . '/' . $record_obj->id;

        // make update data
        $data = factory(VideoPerformance::class)->make()->toArray();
        $data['id'] = $record_obj->id;

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('PUT', $url, $data)
            ->assertStatus(200)
            ->assertJsonStructure($this->standard_json);

        // check to see if the record was updated
        $this->assertDatabaseHas($this->table, $data);
    }
}
