<?php

namespace Tests\Functional;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Dev\Models\User;
use Tests\TestCase;

abstract class FunctionalTestCase extends TestCase
{
    use DatabaseTransactions;

    /**
     * The base API url
     */
    const API_URL = '/api/v1';

    /**
     * @var Authenticatable Reusable user object for authenticated routes
     */
    protected $api_user;

    /**
     * @var string The absolute url used for API calls
     */
    protected $url;

    /**
     * @var array The JSON structure used on GET requests
     */
    protected $pagination_json = [
        'data' => [
            '*' => []
        ],
        'meta' => [
            'current_page',
            'from',
            'last_page',
            'path',
            'per_page',
            'to',
            'total'
        ]
    ];

    /**
     * @var array The JSON structure used on non-GET requests
     */
    protected $standard_json = [
        'data' => []
    ];

    /**
     * The method used by PHPUnit to get things started
     */
    public function setUp()
    {
        parent::setUp();

        $this->api_user = factory(User::class)->create();
    }

    /**
     * Update the json properties in one go
     *
     * @param $json_data
     */
    protected function updateJson($json_data)
    {
        $this->pagination_json['data']['*'] = $json_data;
        $this->standard_json['data'] = $json_data;
    }
}
