<?php

namespace Tests\Functional\User;

use Tests\Functional\FunctionalTestCase;
use App\Dev\Models\User;
use Illuminate\Support\Facades\Storage;

class Base extends FunctionalTestCase
{
    /**
     * The expected structure of the JSON response
     */
    const JSON_STRUCTURE = [
        'first_name',
        'last_name',
        'company_id',
        'email',
        'office_phone',
        'mobile_phone',
        'job_title',
        'address_1',
        'address_2',
        'country_id',
        'city_id',
        'state_id',
        'zip',
        'image',
        'updated_at',
        'created_at',
    ];

    /**
     * @var string  The base url for testing
     */
    protected $route = '/users';

    /**
     * @var string The base table for the current resource
     */
    protected $table = 'users';

    /**
     * Base constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->url = self::API_URL . $this->route;
        $this->updateJson(self::JSON_STRUCTURE);
    }

    public function imageExists(string $path)
    {
        $image_location = User::IMG_DIR . DS . $path;
        Storage::assertExists($image_location);
        // delete the image
        Storage::delete($image_location);
    }

    public function imageMissing(string $path)
    {
        $image_location = User::IMG_DIR . DS . $path;
        Storage::assertMissing($image_location);
    }
}
