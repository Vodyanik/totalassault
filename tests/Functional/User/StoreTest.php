<?php

namespace Tests\Functional\User;

use App\Dev\Models\User;
use Illuminate\Http\UploadedFile;

class StoreTest extends Base
{
    /** @test */
    public function store_record()
    {
        // create contact array
        $record = factory(User::class)->make()->toArray();
        $record['image'] = UploadedFile::fake()->image('image.jpg');
        if (!isset($record['password'])) {
            $record['password'] = bcrypt('secret');
        }

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('POST', $this->url, $record)
            ->assertStatus(201)
            ->assertJsonStructure($this->standard_json);

        // remove the fake images from the insert data
        unset($record['image']);

        // check to see if the record was stored
        $this->assertDatabaseHas($this->table, $record);

        $item = User::where($record)->first();

        // check to see if the image file was created
        $this->imageExists($item->image);
    }
}
