<?php

namespace Tests\Functional\User;

use App\Dev\Models\User;
use Illuminate\Http\UploadedFile;

class UpdateTest extends Base
{
    /** @test */
    public function update_record()
    {
        // create a new record in the database
        $record_obj = factory(User::class)->create();

        // generate the proper url
        $url = $this->url . '/' . $record_obj->id;

        // make update data
        $data = factory(User::class)->make()->toArray();
        $data['image'] = UploadedFile::fake()->image('image.jpg');
        $data['id'] = $record_obj->id;

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('PUT', $url, $data)
            ->assertStatus(200)
            ->assertJsonStructure($this->standard_json);

        // remove the fake images from the insert data
        unset($data['image']);

        // check to see if the record was updated
        $this->assertDatabaseHas($this->table, $data);

        $item = User::where($data)->first();

        // check to see if the image file was created
        $this->imageExists($item->image);

        // make sure the previous image is gone
        $this->imageMissing($record_obj->image);
    }
}
