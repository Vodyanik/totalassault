<?php

namespace Tests\Functional\FbDailyPerformance;

class ListTest extends Base
{
    /** @test */
    public function list_records()
    {
        $this
            ->actingAs($this->api_user, 'api')
            ->json('GET', $this->url)
            ->assertStatus(200)
            ->assertJsonStructure($this->pagination_json);
    }
}
