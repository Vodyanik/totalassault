<?php

namespace Tests\Functional\FbDailyPerformance;

use App\Dev\Models\Facebook\DailyPerformance;

class ShowTest extends Base
{
    /** @test */
    public function show_record()
    {
        // create record
        $record = factory(DailyPerformance::class)->create()->first();

        // generate the proper url
        $url = $this->url . '/' . $record->id;

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('GET', $url)
            ->assertStatus(200)
            ->assertJsonStructure($this->standard_json);
    }
}
