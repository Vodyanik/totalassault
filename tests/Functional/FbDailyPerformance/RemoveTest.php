<?php

namespace Tests\Functional\FbDailyPerformance;

use App\Dev\Models\Facebook\DailyPerformance;

class RemoveTest extends Base
{
    /** @test */
    public function delete_record()
    {
        // persist a new record in the database
        $record_obj = factory(DailyPerformance::class)->create();
        $record = $record_obj->toArray();

        // generate the proper url
        $url = $this->url . '/' . $record_obj->id;

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('DELETE', $url, $record)
            ->assertStatus(200)
            ->assertJsonStructure($this->standard_json);

        // check to see if the table record was deleted
        $this->assertDatabaseMissing($this->table, $record);
    }
}
