<?php

namespace Tests\Functional\FbDailyPerformance;

use Tests\Functional\FunctionalTestCase;

class Base extends FunctionalTestCase
{
    /**
     * The expected structure of the JSON response
     */
    const JSON_STRUCTURE = [
        'ad_id',
        'adset_id',
        'user_id',
        'campaign_id',
        'date_start',
        'date_stop',
        'spend',
        'purchases',
        'cpa',
        'conv_value',
        'net',
        'roas',
        'clicks',
        'cpc',
        'impressions',
        'checkout_int',
        'updated_at',
        'created_at',
    ];

    /**
     * @var string  The base url for testing
     */
    protected $route = '/advertising/facebook/daily-performance';

    /**
     * @var string The base table for the current resource
     */
    protected $table = 'fb_daily_performance';

    /**
     * Base constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->url = self::API_URL . $this->route;
        $this->updateJson(self::JSON_STRUCTURE);
    }
}
