<?php

namespace Tests\Functional\Project;

use App\Dev\Models\Project;
use Illuminate\Http\UploadedFile;

class UpdateTest extends Base
{
    /** @test */
    public function update_record()
    {
        // create a new record in the database
        $record_obj = factory(Project::class)->create();

        // generate the proper url
        $url = $this->url . '/' . $record_obj->id;

        // make update data
        $data = factory(Project::class)->make()->toArray();
        $data['image'] = UploadedFile::fake()->image('image.jpg');
        $data['image_2'] = UploadedFile::fake()->image('image_2.jpg');
        $data['id'] = $record_obj->id;

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('PUT', $url, $data)
            ->assertStatus(200)
            ->assertJsonStructure($this->standard_json);

        // remove the fake images from the insert data
        unset($data['image']);
        unset($data['image_2']);

        // check to see if the record was updated
        $this->assertDatabaseHas($this->table, $data);

        $item = Project::where($data)->first();

        // check to see if the image files were created
        $this->imageExists($item->image);
        $this->imageExists($item->image_2);

        // make sure the previous project images are gone
        $this->imageMissing($record_obj->image);
        $this->imageMissing($record_obj->image_2);
    }
}
