<?php

namespace Tests\Functional\Project;

use Tests\Functional\FunctionalTestCase;
use App\Dev\Models\Project;
use Illuminate\Support\Facades\Storage;

class Base extends FunctionalTestCase
{
    /**
     * The expected structure of the JSON response
     */
    const JSON_STRUCTURE = [
        'name',
        'image',
        'image_2',
        'facebook_id',
        'twitter_id',
        'instagram_id',
        'youtube_id',
        'soundcloud_id',
        'publicity_status',
        'publicity_bc_team_id',
        'assets',
        'status',
        'updated_at',
        'created_at',
    ];

    /**
     * @var string  The base url for testing
     */
    protected $route = '/projects';

    /**
     * @var string The base table for the current resource
     */
    protected $table = 'projects';

    /**
     * Base constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->url = self::API_URL . $this->route;
        $this->updateJson(self::JSON_STRUCTURE);
    }

    public function imageExists(string $path)
    {
        $image_location = Project::IMG_DIR . DS . $path;
        Storage::assertExists($image_location);
        // delete the image
        Storage::delete($image_location);
    }

    public function imageMissing(string $path)
    {
        $image_location = Project::IMG_DIR . DS . $path;
        Storage::assertMissing($image_location);
    }
}
