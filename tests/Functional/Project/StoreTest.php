<?php

namespace Tests\Functional\Project;

use App\Dev\Models\Project;
use Illuminate\Http\UploadedFile;

class StoreTest extends Base
{
    /** @test */
    public function store_record()
    {
        // create contact array
        $record = factory(Project::class)->make()->toArray();
        $record['image'] = UploadedFile::fake()->image('image.jpg');
        $record['image_2'] = UploadedFile::fake()->image('image_2.jpg');

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('POST', $this->url, $record)
            ->assertStatus(201)
            ->assertJsonStructure($this->standard_json);

        // remove the fake images from the insert data
        unset($record['image']);
        unset($record['image_2']);

        // check to see if the record was stored
        $this->assertDatabaseHas($this->table, $record);

        $item = Project::where($record)->first();

        // check to see if the image file was created
        $this->imageExists($item->image);

        // check to see if the image_2 file was created
        $this->imageExists($item->image_2);
    }
}
