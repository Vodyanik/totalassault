<?php

namespace Tests\Functional\FbPage;

use Tests\Functional\FunctionalTestCase;

class Base extends FunctionalTestCase
{
    /**
     * The expected structure of the JSON response
     */
    const JSON_STRUCTURE = [
        'name',
        'username',
        'link',
        'category_id',
        'access_token',
        'auth_user_id',
        'auth_user_name',
        'auth_user_token',
        'status',
        'updated_at',
        'created_at',
    ];

    /**
     * @var string  The base url for testing
     */
    protected $route = '/advertising/facebook/pages';

    /**
     * @var string The base table for the current resource
     */
    protected $table = 'fb_pages';

    /**
     * Base constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->url = self::API_URL . $this->route;
        $this->updateJson(self::JSON_STRUCTURE);
    }
}
