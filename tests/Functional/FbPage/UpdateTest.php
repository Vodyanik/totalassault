<?php

namespace Tests\Functional\FbPage;

use App\Dev\Models\Facebook\Page;

class UpdateTest extends Base
{
    /** @test */
    public function update_record()
    {
        // create a new record in the database
        $record_obj = factory(Page::class)->create();
        $record_id = $record_obj->id;

        // generate the proper url
        $url = $this->url . '/' . $record_obj->id;

        // make update data
        $data = factory(Page::class)->make()->toArray();
        $data['id'] = $record_id;

        // check the API route
        $this
            ->actingAs($this->api_user, 'api')
            ->json('PUT', $url, $data)
            ->assertStatus(200)
            ->assertJsonStructure($this->standard_json);

        // check to see if the record was updated
        $this->assertDatabaseHas('fb_pages', $data);
    }
}
