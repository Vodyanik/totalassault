<?php

declare(strict_types = 1);

namespace Tests\Unit;

use Tests\TestCase;
use App\Dev\Services\CsvService;
use App\Dev\Repositories\FbDailyPerformanceRepository as Repository;

class CsvServiceTest extends TestCase
{
    protected $service;
    protected $repository;

    public function setUp()
    {
        parent::setUp();

        $this->service = new CsvService();
        $this->repository = new Repository();
    }

    /** @test */
    public function get_data_between_dates()
    {
        $request_data = [
            'limit' => 20,
            'page' => 1,
        ];
        $request_data['date'] = [format_date_for_mysql('2018-01-01'), format_date_for_mysql('2020-01-01')];
        $records = $this->service->getRecords($request_data, $this->repository)->toArray()['data'];
        $expected_records = $this
            ->repository
            ->whereBetween('created_at', $request_data['date'])
            ->paginate($request_data['limit'])->toArray()['data'];

        $this->assertEquals(count($records), count($expected_records));
        $this->assertEquals(array_values($records), array_values($expected_records));
    }

    /** @test */
    public function get_data_without_dates()
    {
        $request_data = [
            'limit' => 20,
            'page' => 1,
        ];
        $records = $this->service->getRecords($request_data, $this->repository)->toArray()['data'];
        $expected_records = $this->repository->paginate($request_data['limit'])->toArray()['data'];

        $this->assertEquals(count($records), count($expected_records));
        $this->assertEquals(array_values($records), array_values($expected_records));
    }

    /** @test */
    public function get_the_selected_columns_only()
    {
        $request_data = [
            'limit' => 20,
            'page' => 1,
            'columns' => [
                'id', 'created_at', 'updated_at'
            ]
        ];
        $records = $this->service->getRecords($request_data, $this->repository)->toArray()['data'];
        $first_record_keys = array_keys($records[0]);

        $this->assertEquals(count($first_record_keys), count($request_data['columns']));
        $this->assertEquals($first_record_keys, $request_data['columns']);
    }

    /** @test */
    public function get_all_available_columns()
    {
        $request_data = [
            'limit' => 20,
            'page' => 1,
        ];
        $records = $this->service->getRecords($request_data, $this->repository)->toArray()['data'];
        $expected_records = $this->repository->paginate($request_data['limit'])->toArray()['data'];

        $first_record_keys = array_keys($records[0]);
        $first_expected_record_keys = array_keys($expected_records[0]);

        $this->assertEquals(count($first_record_keys), count($first_expected_record_keys));
        $this->assertEquals($first_record_keys, $first_expected_record_keys);
    }

    /** @test */
    public function get_paginated_data()
    {
        $request_data = [
            'limit' => 3,
            'page' => 1,
        ];
        $records = $this->service->getRecords($request_data, $this->repository)->toArray()['data'];
        $this->assertEquals(count($records), $request_data['limit']);
    }

    /** @test */
    public function get_all_data_without_limit()
    {
        $request_data = [];
        $records = $this->service->getRecords($request_data, $this->repository)->toArray();
        $expected_records = $this->repository->get()->toArray();

        $this->assertEquals(count($records), count($expected_records));
        $this->assertEquals(array_values($records), array_values($expected_records));
    }

    /** @test */
    public function get_all_data_with_null_limit()
    {
        $request_data = [
            'limit' => 'null'
        ];
        $records = $this->service->getRecords($request_data, $this->repository)->toArray();
        $expected_records = $this->repository->get()->toArray();

        $this->assertEquals(count($records), count($expected_records));
        $this->assertEquals(array_values($records), array_values($expected_records));
    }

    protected function deleteFile($location)
    {
        if (file_exists($location)) {
            unlink($location);
        }
    }

    /** @test */
    public function create_csv_file()
    {
        $request_data = [
            'limit' => 20,
            'page' => 1,
        ];
        $records = $this->service->getRecords($request_data, $this->repository);
        $file_path = $this->service->createFile($records);
        $basename = basename($file_path);
        $extension = explode('.', $basename)[1];

        $this->assertFileExists($file_path);
        $this->assertEquals('csv', $extension);

        $this->deleteFile($file_path);
    }

    /** @test */
    public function column_names_exist_in_csv_file()
    {
        $request_data = [
            'limit' => 20,
            'page' => 1,
        ];
        $records = $this->service->getRecords($request_data, $this->repository);
        $file_path = $this->service->createFile($records);
        $columns = array_keys($records[0]->getAttributes());
        $file_contents = file($file_path);
        $first_line_of_content = str_replace("\n", '', $file_contents[0]);
        $first_line_of_content_array = explode(',', $first_line_of_content);

        $this->assertEquals($columns, $first_line_of_content_array);
        $this->assertEquals(count($columns), count($first_line_of_content_array));

        $this->deleteFile($file_path);
    }

    /** @test */
    public function data_exists_in_csv_file()
    {
        $request_data = [
            'limit' => 20,
            'page' => 1,
        ];
        $records = $this->service->getRecords($request_data, $this->repository);
        $file_path = $this->service->createFile($records);
        $records_array = $records->toArray()['data'];
        $file_contents = file($file_path);

        // delete the column names
        unset($file_contents[0]);

        foreach ($file_contents as $key => $line) {
            $line = str_replace("\n", '', $line);
            $line = explode(',', $line);
            $expected_line = array_values($records_array[$key - 1]);

            $this->assertEquals(count($expected_line), count($line));
            $this->assertEquals($expected_line[0], $line[0]);
        }

        $this->deleteFile($file_path);
    }
}
